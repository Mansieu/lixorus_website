-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 05, 2018 at 05:30 PM
-- Server version: 5.7.22-0ubuntu0.16.04.1
-- PHP Version: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `website`
--

-- --------------------------------------------------------

--
-- Table structure for table `dictionnary`
--

CREATE TABLE `dictionnary` (
  `language` int(2) NOT NULL,
  `aindex` varchar(255) NOT NULL,
  `lang` text NOT NULL,
  `action` varchar(255) NOT NULL,
  `app` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dictionnary`
--

INSERT INTO `dictionnary` (`language`, `aindex`, `lang`, `action`, `app`) VALUES
(2, '404:title', 'These are not the droids you are looking for', '404', 'website'),
(2, '404:lead', 'Sometimes even the best people get lost. But don\'t worry, I get it. That map makes no sense to me neither. Maybe if you tilt it to the left... ?', '404', 'website'),
(2, '404:text', 'Obviously, this ain\'t where you wanted to go. The page is no longer available, or is currently either under construction or modification. This means you can\'t access it for the moment. For more information about it, you can contact the owner of the website. And before you ask, no, we do not have pages with cute cat pictures, you\'ve got the wrong website alltogether.', '404', 'website'),
(1, '404:title', 'Ce ne sont pas les droides que vous recherchez', '404', 'website'),
(1, 'base:title', 'Fichier introuvable', '404', 'website'),
(1, 'footer:cgu', 'Condition g&eacute;n&eacute;rale d\'utilisation', 'footer', 'website'),
(1, 'menu:home', 'Accueil', 'menu', 'website'),
(1, 'menu:news', 'Actualit&eacute;s', 'menu', 'website'),
(1, 'menu:login', 'Connexion', 'menu', 'website'),
(1, 'menu:signin', 'Inscription', 'menu', 'website'),
(1, '404:link', 'Retourner &agrave; l\'accueil', '404', 'website'),
(1, '404:lead', 'Parfois, m&ecirc;me les meilleurs d\'entre nous se perdent. Pas d\'inqui&eacute;tude, je te comprends. C\'est pas facile de lire une carte. Peut-&ecirc;tre en la penchant un peu vers la gauche... ?', '404', 'website'),
(1, '404:text', 'Manifestement, ce n\'est pas ce que vous recherchiez. Cette page n\'est plus disponible, ou est en cours de contruction ou modification. Elle n\'est donc pour le moment pas accessible. Pour plus d\'informations, vous pouvez contacter le propri&eacute;taire du site. Et avant que vous le demandiez, non, nous n\'avons pas de page avec des photos de chat mignonnes, vous vous &ecirc;tes tromp&eacute; de site.', '404', 'website'),
(1, 'base:title', 'Accueil', 'home', 'website'),
(1, 'base:title', 'Actualit&eacute;s', 'news', 'website'),
(1, 'news:search:input', 'taper et entrer', 'news', 'website'),
(1, 'news:login:label', 'Connexion', 'news', 'website'),
(1, 'news:article:none', 'Aucun article disponible', 'news', 'website'),
(1, 'news:latestnews:title', 'Derniers articles', 'news', 'website'),
(1, 'news:read', 'Lire', 'news', 'website'),
(1, 'rules:rules:content', '\r\n                    <img src="ressources/rules/pseudo.jpg" alt="pseudo" />\r\n\r\n                    <p class="header">En vous inscrivant sur le serveur, vous vous engagez &agrave; respecter un certain nombre de r&egrave;gles. Celles-ci sont l&agrave; pour maintenir une ambiance conviviale et optimiser le confort de jeu de chaque joueur. Dans le cas o&ugrave; un joueur choisit de contrevenir &agrave; ces r&egrave;gles, il doit savoir qu\'il s\'expose &agrave; des sanctions. Si vous constatez un manquement &agrave; ces r&egrave;gles, vous pouvez le signaler sur le support.</p>\r\n\r\n                    <p class="header">Les r&egrave;gles list&eacute;es ici ne sont pas exhaustives : les membres de l\'&eacute;quipe sont libres d\'utiliser leur bon sens pour sanctionner un comportement inacceptable qui ne serait pas explicitement proscrit dans le r&egrave;glement.</p>\r\n\r\n                    <hr />\r\n\r\n                    <h3>PSEUDONYMES DE COMPTES ET PERSONNAGES</h3>\r\n\r\n                    <p>Un pseudonyme de compte, de personnage et un nom de guilde doit respecter les r&egrave;gles suivantes :</p>\r\n\r\n                    <ul>\r\n                        <li>Correspondre &agrave; un mot pronon&ccedil;able (par exemple le mot zpribjhneob ne respecte pas cette r&egrave;gle)</li>\r\n                        <li>Ne pas faire r&eacute;f&eacute;rence &agrave; une orientation politique, &agrave; une ethnie, une communaut&eacute; ou une religion</li>\r\n                        <li>Ne pas &ecirc;tre vulgaire ni insultant</li>\r\n                        <li>Ne pas comporter de connotation sexuelle voir pornographique</li>\r\n                        <li>Ne pas contenir votre nom et/ou pr&eacute;nom</li>\r\n                        <li>Ne pas faire r&eacute;f&eacute;rence &agrave; une marque d&eacute;pos&eacute;e</li>\r\n                        <li>Ne pas faire r&eacute;f&eacute;rence &agrave; un produit stup&eacute;fiant ou &agrave; toute autre entit&eacute; interdite par la l&eacute;gislation</li>\r\n                        <li>Ne pas sugg&eacute;rer des pratiques interdites par les r&egrave;gles du jeu</li>\r\n                        <li>Ne pas imiter les noms de personnages du lore du serveur ou d\'Ankama en g&eacute;n&eacute;ral (personnages non joueurs, h&eacute;ros de l\'histoire...)</li>\r\n                        <li>Ne pas imiter le nom d\'un mod&eacute;rateur ou d\'un membre de l\'&eacute;quipe</li>\r\n                        <li>Ne pas &ecirc;tre orthographi&eacute; ou &eacute;pel&eacute; alternativement dans le but de contourner les r&egrave;gles impos&eacute;es ci-dessus.</li>\r\n                    </ul>\r\n\r\n                    <h3>CANAUX DE DISCUSSION</h3>\r\n\r\n                    <p>Le flood, consistant &agrave; r&eacute;p&eacute;ter le m&ecirc;me message plusieurs fois dans un court d&eacute;lai n\'est pas autoris&eacute;</p>\r\n                    <p>Chaque canal a sa fonction. Il est d&eacute;fendu d\'utiliser un canal &agrave; mauvais escient.</p>\r\n                    <p>Il est interdit d\'abuser de caract&egrave;res sp&eacute;ciaux, d\'abuser du capslock (message en majuscules), de r&eacute;p&eacute;ter un caract&egrave;re dans l\'optique d\'attirer l\'attention.</p>\r\n                    <p>Il est interdit de faire de la publicit&eacute; en canal g&eacute;n&eacute;ral / recrutement / commerce / politique pour un site tiers, y compris ceux en rapport avec Ankama (et ce afin d\'&eacute;viter tout probl&egrave;me d\'arnaque ou de phishing).</p>\r\n\r\n                    <h3>RESPECT DES AUTRES JOUEURS</h3>\r\n\r\n                    <p>Afin de conserver une ambiance conviviale sur les serveurs, la vulgarit&eacute;, les insultes, les discours racistes, x&eacute;nophobes, homophobes, sexuels et plus g&eacute;n&eacute;ralement tous les propos d&eacute;plac&eacute;s sont interdits.</p>\r\n                    <p>Chaque serveur est associ&eacute; &agrave; une langue. Il est demand&eacute; d\'utiliser uniquement celle-ci sur les canaux publics. Vous &ecirc;tes toutefois libre de parler dans une autre langue en priv&eacute;.</p>\r\n                    <p>Les arnaques sont prohib&eacute;es. Tout constat effectu&eacute; par un mod&eacute;rateur entra&icirc;nera une sanction imm&eacute;diate.</p>\r\n                    <p>L\'usurpation d\'identit&eacute; est interdite, que ce soit celle d\'un joueur, d\'une guilde, d\'un mod&eacute;rateur, ou d\'un membre d\'Ankama, et ce peu importe l\'intention (qu\'elle soit bonne ou mauvaise).</p>\r\n                    <p>Dans un souci de confidentialit&eacute;, d&eacute;voiler les informations personnelles d\'un autre joueur est prohib&eacute;.</p>\r\n                    <p>De mani&egrave;re g&eacute;n&eacute;rale, il est interdit de nuire au bon d&eacute;roulement du jeu des autres joueurs d\'aucune mani&egrave;re que ce soit.</p>\r\n\r\n                    <h3>SIGNALEMENT</h3>\r\n\r\n                    <p>Le signalement abusif ou mensonger d\'une infraction, d\'un bug, ou autres messages r&eacute;p&eacute;t&eacute;s sur le support ou directement &agrave; un membre de l\'&eacute;quipe sur l\'une des plateformes disponibles est interdit. Il sera si n&eacute;cessaire gravement sanctionn&eacute;.</p>\r\n                    <p>La relance abusive de ticket du support est interdite.</p>', 'rules', 'website'),
(1, 'base:title', 'Tableau de bord', 'home', 'admin'),
(1, 'login:title', 'CONNECTION', 'login', 'website'),
(1, 'login:formular:account', 'Nom de compte', 'login', 'website'),
(1, 'login:formular:password', 'Mot de passe', 'login', 'website'),
(1, 'signin:title', 'INSCRIPTION', 'login', 'website'),
(1, 'signin:formular:account', 'Nom de compte', 'login', 'website'),
(1, 'signin:formular:nickname', 'Pseudonyme', 'login', 'website'),
(1, 'signin:formular:secretquestion', 'Question secr&egrave;te', 'login', 'website'),
(1, 'signin:formular:email', 'Email', 'login', 'website'),
(1, 'signin:formular:password', 'Mot de passe', 'login', 'website'),
(1, 'signin:formular:password2', 'Confirmer le mot de passe', 'login', 'website'),
(1, 'signin:formular:secretanswer', 'R&eacute;ponse secr&egrave;te', 'login', 'website'),
(1, 'signin:formular:submit', 'Valider', 'login', 'website'),
(1, 'rules:rules:title', 'R&egrave;gles', 'rules', 'website'),
(1, 'rules:sanctions:title', 'Sanctions', 'rules', 'website'),
(1, 'rules:sanctions:content', '\r\n                    <img src="ressources/rules/sanctions.jpg" alt="sanctions" />\r\n\r\n                    <p class="header">Certaines de ces sanctions peuvent &ecirc;tre appliqu&eacute;es automatiquement par le serveur. Si cela est le cas, il vous le sera toujours indiqu&eacute; dans le message d\'avertissement ou de signalement de sanction. Dans le cas contraire, cela signifit la plupart du temps que vous avez &eacute;t&eacute; sanctionn&eacute; par un &ecirc;tre humain.</p>\r\n\r\n                    <p class="header">Si vous souhaitez contester une sanction, vous pouvez vous rendre sur le support, o&ugrave; un autre membre de l\'&eacute;quipe s\'occupera de votre probl&egrave;me et servira de m&eacute;diateur si n&eacute;cessaire.</p>\r\n\r\n                    <hr />\r\n\r\n                    <p>Voici ci-dessous une liste non-exhaustive de sanctions pouvant &ecirc;tre appliqu&eacute;es.</p>\r\n\r\n                    <h3>L\'AVERTISSEMENT</h3>\r\n\r\n                    <p>Lors de certaines infractions, un avertissement peut vous &ecirc;tre envoy&eacute;. Cet avertissement est avant tout envoy&eacute; pour informer le joueur des r&egrave;gles et le pr&eacute;venir qu\'il risque une sanction s\'il continue &agrave; enfreindre les CGU. Ce message est envoy&eacute; directement en jeu par un mod&eacute;rateur ou par mail via le Support. Celui-ci est consid&eacute;r&eacute; comme lu et compris par le joueur.</p>\r\n\r\n                    <h3>LE MUTE</h3>\r\n\r\n                    <p>La privation de parole est une des sanctions appliqu&eacute;es par les mod&eacute;rateurs &agrave; l\'encontre des joueurs qui ne respectent pas les r&egrave;gles du serveur. Elle peut &ecirc;tre accompagn&eacute;e d\'une demande de suspension de compte aupr&egrave;s du Support.</p>\r\n\r\n                    <h3>LE KICK</h3>\r\n\r\n                    <p>L\'expulsion du jeu pour un temps donn&eacute; est appliqu&eacute;e par les mod&eacute;rateurs pour les cas plus graves. Elle est g&eacute;n&eacute;ralement appliqu&eacute;e &agrave; titre dissuasif ou accompagn&eacute;e d\'une demande de suspension de compte aupr&egrave;s du Support.</p>\r\n\r\n                    <h3>LA SUSPENSION TEMPORAIRE</h3>\r\n\r\n                    <p>Suite &agrave; une demande de suspension, le Support peut d&eacute;cider de bloquer l\'acc&egrave;s &agrave; votre compte pour plusieurs jours (3, 7, 15 ou 30 jours). Cette suspension concerne g&eacute;n&eacute;ralement les cas de r&eacute;cidive, et pr&eacute;c&egrave;de parfois le bannissement d&eacute;finitif. Sa dur&eacute;e d&eacute;pendra de la gravit&eacute; de la faute.</p>\r\n\r\n                    <h3>LA SUSPENSION D&eacute;FINITIVE</h3>\r\n\r\n                    <p>Le compte est bloqu&eacute; d&eacute;finitivement, le joueur ne peut donc plus y acc&eacute;der. Cette sanction est appliqu&eacute;e pour les joueurs multir&eacute;cidivistes, ou en cas de manquement impardonnable aux r&egrave;gles du serveur.</p>\r\n                    <p>De mani&egrave;re g&eacute;n&eacute;rale, plusieurs avertissements conduiront &agrave; un bannissement et plusieurs bannissements entra&icirc;neront une suspension d&eacute;finitive. Certaines infractions donneront toutefois lieu &agrave; un bannissement d&eacute;finitif imm&eacute;diat.</p>', 'rules', 'website'),
(1, 'rules:selling:title', 'Ventes', 'rules', 'website'),
(1, 'rules:selling:content', '\r\n                    <img src="ressources/rules/phishing.jpg" alt="phishing" />\r\n\r\n                    <h3>PR&ecirc;T, PARTAGE, DON, VENTE / ACHAT DE COMPTE</h3>\r\n\r\n                    <p>En raison des divers abus possibles, il est interdit de partager ou pr&ecirc;ter son compte. En cas de manquement &agrave; cette r&egrave;gle, les deux protagonistes (pr&ecirc;teur et receveur) encourent des sanctions.</p>\r\n\r\n                    <p>La vente, l\'achat, l\'&eacute;change et le don de comptes sont &eacute;galement interdits. Les comptes sont la propri&eacute;t&eacute; du serveur et, &agrave; ce titre, aucun profit p&eacute;cuniaire, que ce soit en monnaie r&eacute;elle ou virtuelle, ne peut en &ecirc;tre tir&eacute;. De plus, un compte &eacute;tant associ&eacute; &agrave; des donn&eacute;es personnelles, il est imprudent de les divulguer. Le propri&eacute;taire originel du compte reste toujours responsable des actions de son personnage.</p>\r\n\r\n                    <p>En cas de perte, sanction ou autres, le propri&eacute;taire du compte ainsi que ceux ayant particip&eacute;s &agrave; l\'infraction pourront &ecirc;tre tenus responsables et sanctionn&eacute;s.</p>\r\n\r\n                    <h3>COMMERCE</h3>\r\n\r\n                    <p>Le commerce de compte, de guilde est interdit</p>\r\n                    <p>Le commerce d\'abonnement et de kamas est interdit</p>', 'rules', 'website'),
(1, 'rules:cheating:title', 'Triche', 'rules', 'website'),
(1, 'rules:cheating:content', '\r\n                    <img src="ressources/rules/arnaques.jpg" alt="arnaques" />\r\n\r\n                    <h3>TRICHE</h3>\r\n\r\n                    <p>Il est interdit d\'abuser d\'un bug du jeu. Toute anomalie doit &ecirc;tre report&eacute;e au Support dans la section correspondante.</p>\r\n                    <p>L\'utilisation de programme tiers (type " bot ") est interdite.</p>\r\n                    <p>La modification du client du serveur est interdite. Ceci englobe tous les fichiers pr&eacute;sents dans le r&eacute;pertoire d\'installation du serveur.</p>\r\n                    <p>Il est interdit de tricher lors de la participation &agrave; une animation.</p>\r\n                    <p>Les mod&eacute;rateurs sont les garants du respect de ces r&egrave;gles en jeu. Ils ne constituent en aucun cas un palliatif au Support. Le non-respect des r&egrave;gles &eacute;nonc&eacute;es ci-dessus vous expose &agrave; des sanctions imm&eacute;diates et/ou diff&eacute;r&eacute;es pouvant se traduire par des avertissements individuels ou de groupe. Ceux-ci peuvent aboutir &agrave; une exclusion d&eacute;finitive de vos comptes.</p>', 'rules', 'website'),
(1, 'rules:animation:title', 'Animation', 'rules', 'website'),
(1, 'rules:animation:content', '\r\n                    <img src="ressources/rules/modos.jpg" alt="modos" />\r\n\r\n                    <h3>ANIMATIONS</h3>\r\n\r\n                    <p>Faire obstruction, g&ecirc;ner, nuire de mani&egrave;re volontaire au bon d&eacute;roulement d\'un &eacute;v&egrave;nement en jeu organis&eacute; par un membre de l\'&eacute;quipe du serveur, avec l\'autorisation d\'un membre de l\'&eacute;quipe du serveur ou par des joueurs est interdit. Toute entorse peut entra&icirc;ner des sanctions imm&eacute;diates.</p>\r\n\r\n                    <p>L\'organisation, sans autorisation, de loteries faisant appel &agrave; la g&eacute;n&eacute;rosit&eacute; des joueurs pour la constitution des lots est interdite, le risque de vol ou d\'arnaque &eacute;tant trop &eacute;lev&eacute;.</p>\r\n\r\n                    <h3>ANIMATIONS ORGANISEES PAR UN JOUEUR</h3>\r\n\r\n                    <p>L\'organisation d\'animations ne n&eacute;cessite pas l\'autorisation d\'un membre de l\'&eacute;quipe du serveur, mais elle est toutefois sujette &agrave; certaines r&eacute;serves.</p>\r\n\r\n                    <p>Il est pr&eacute;f&eacute;rable qu\'elle soit organis&eacute;e en avance, afin de faciliter son d&eacute;roulement et de limiter les d&eacute;bordements.</p>\r\n\r\n                    <p>Une animation ne peut en aucun cas porter atteinte &agrave; un joueur n\'y participant pas. Les chasses &agrave; l\'homme, aggressions gratuites ou tout autre action succeptible de porter pr&eacute;judice &agrave; un autre joueur sont interdites. L\'organisateur et/ou les participants pourront se voir sanctionner.</p>\r\n\r\n                    <p>Il est possible de demander &agrave; un membre de l\'&eacute;quipe du serveur de l\'aide pour l\'organisation ou le d&eacute;roulement d\'une animation. Il est possible de demander &agrave; un membre de l\'&eacute;quipe du serveur l\'autorisation de participer &agrave; l\'organisation d\'une animation d\'un autre joueur ou dudit membre de l\'&eacute;quipe. Il est &eacute;galement possible qu\'il refuse.</p>\r\n\r\n                    <p>Il n\'est pas n&eacute;cessaire de proposer une r&eacute;compense en fin d\'animation. Il est en revanche fortement conseill&eacute; de le pr&eacute;ciser le cas &eacute;ch&eacute;ant. Tout d&eacute;bordement volontairement caus&eacute; de cette mani&egrave;re pourra voir son organisateur sanctionn&eacute;.</p>\r\n\r\n                    <p>Il est interdit de refuser la participation d\'un joueur &agrave; une animation. Seule exception possible : un joueur d\'un niveau inadapt&eacute; pour une animation qui n&eacute;cessite un niveau ou une tranche de niveau pr&eacute;cise pourra &ecirc;tre refus&eacute; par l\'organisateur. Dans le cas d\'un joueur turbulent ou r&eacute;guli&egrave;rement fauteur de trouble, il est possible de demander conseil &agrave; un membre de l\'&eacute;quipe du serveur.</p>', 'rules', 'website'),
(1, 'ladder:table:name', 'Nom', 'ladder', 'website'),
(1, 'ladder:table:level', 'Niveau', 'ladder', 'website'),
(1, 'ladder:table:head', 'Meneur', 'ladder', 'website'),
(1, 'ladder:table:rank', '#', 'ladder', 'website'),
(1, 'ladder:table:members', 'Membres', 'ladder', 'website'),
(1, 'ladder:table:experience', 'Exp&eacute;rience', 'ladder', 'website'),
(1, 'ladder:table:race', 'Classe', 'ladder', 'website'),
(1, 'ladder:table:guild', 'Guilde', 'ladder', 'website'),
(1, 'ladder:table:won', 'Win', 'ladder', 'website'),
(1, 'ladder:table:lost', 'Lost', 'ladder', 'website'),
(1, 'ladder:table:ratio', 'Ratio', 'ladder', 'website'),
(1, 'support-new:form:ticket-type:title', 'Type de ticket', 'support-new', 'website'),
(1, 'support-new:form:ticket-type:label', 'Choisir le type de ticket', 'support-new', 'website'),
(1, 'support-new:form:ticket-type:value:bug', 'Signaler un bug ou un probl&egrave;me', 'support-new', 'website'),
(1, 'support-new:form:ticket-type:value:problem-buying', 'Signaler un probl&egrave;me lors d\'un achat', 'support-new', 'website'),
(1, 'support-new:form:ticket-type:value:offense', 'Signaler une infraction', 'support-new', 'website'),
(1, 'support-new:form:ticket-type:value:offense-team', 'Signaler un membre de l\'&eacute;quipe', 'support-new', 'website'),
(1, 'support-new:form:ticket-type:value:contest', 'Contester une sanction', 'support-new', 'website'),
(1, 'support-new:form:ticket-type:value:ask', 'Faire une demande', 'support-new', 'website'),
(1, 'support-new:spam-warning', 'L\'envoi abusif de tickets est passible de sanction. Consultez le r&egrave;glement pour en savoir plus.', 'support-new', 'website'),
(1, 'support-new:form:send', 'Envoyer', 'support-new', 'website'),
(1, 'support-new:form:location:label', 'Id de la carte et de la cellule concern&eacute;e (et/ou g&eacute;oposition)', 'support-new:', 'website'),
(1, 'support-new:form:time:placeholder', 'Mardi 12 Juillet 1962 &agrave; 21h26 et 32 secondes', 'support-new:', 'website'),
(1, 'support-new:form:time:label', 'Date et heure (aussi pr&eacute;cis&eacute;ment que possible)', 'support-new:', 'website'),
(1, 'support-new:form:nickname:placeholder', 'Marcel-lebogoce', 'support-new:', 'website'),
(1, 'support-new:form:nickname:label', 'Pseudonyme du personnage concern&eacute;', 'support-new:', 'website'),
(1, 'support-new:form:location:placeholder', 'Mapid : 666, Cellid : 42, Pos : 666,666, derri&egrave;re une des portes', 'support-new:', 'website'),
(1, 'support-new:form:screenshot:label', 'Screenshot', 'support-new:', 'website'),
(1, 'support-new:form:screenshot:placeholder', 'http://i50.tinypic.com/w7ezo9.png', 'support-new:', 'website'),
(1, 'support-new:form:description:label', 'Description', 'support-new:', 'website');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dictionnary`
--
ALTER TABLE `dictionnary`
  ADD PRIMARY KEY (`language`,`aindex`,`action`,`app`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
