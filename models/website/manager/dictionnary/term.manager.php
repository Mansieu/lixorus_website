<?php

require_once(MODELS_WEBSITE_MANAGER."manager.inc.php");
require_once(MODELS_WEBSITE_CLASS."dictionnary/term.class.php");

class Term_Manager extends Manager
{
    public static $sClass = "Term";
    public static $sTable = "dictionnary";
    public static $sPrimary = "language, action, aindex";

    public static $aStructure = array(
        "iLanguage"    => array("attname" => "language",    "default" => false,      "value" => ""),
        "sAction"      => array("attname" => "action",      "default" => false,      "value" => ""),
        "sIndex"       => array("attname" => "aindex",      "default" => false,      "value" => ""),
        "sValue"       => array("attname" => "lang",        "default" => false,      "value" => ""),
        "sApp"         => array("attname" => "app",         "default" => false,      "value" => ""),
    );

    public static function getCurrent($oDb, $iLangage, $sApp, $sAction)
    {
        $aParams = array(
            "where" => array(
                "iLanguage" => array(
                    "operation" => "=",
                    "value" => $iLangage,
                ),
                "sAction" => array(
                    "operation" => "=",
                    "value" => $sAction,
                ),
                "sApp" => array(
                    "operation" => "=",
                    "value" => $sApp,
                ),
            ),
        );
        return self::select($oDb, self::$sClass, $aParams);
    }

    public static function getAll($oDb)
    {
        $aParams = array();
        return self::select($oDb, self::$sClass, $aParams);
    }
}
