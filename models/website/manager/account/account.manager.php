<?php

require_once(MODELS_WEBSITE_MANAGER."manager.inc.php");
require_once(MODELS_WEBSITE_CLASS."account/account.class.php");

class Account_Manager extends Manager
{
    public static $sClass = "Account";
    public static $sTable = "account";
    public static $sPrimary = "id";

    public static $aStructure = array(
        "iId"        => array("attname" => "id",         "default" => false,     "value" => ""),
        "sUserName"  => array("attname" => "username",   "default" => false,     "value" => ""),
        "sPassword"  => array("attname" => "password",   "default" => false,     "value" => ""),
        "sSalt"      => array("attname" => "salt",       "default" => false,     "value" => ""),
        "sEmail"     => array("attname" => "email",      "default" => false,     "value" => ""),
        "sKey"       => array("attname" => "code",       "default" => false,     "value" => ""),
        "iRank"      => array("attname" => "rank",       "default" => true,      "value" => "1"),
    );

    public static function get($oDb, $sValue = "")
    {
        $aParams = array(
            "where" => array(
                "iId" => array(
                    "operation" => "=",
                    "value" => $sValue,
                ),
            ),
            "offset" => 0,
            "quantity" => 1,
        );
        return self::select($oDb, self::$sClass, $aParams);
    }
}
