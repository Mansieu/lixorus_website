<?php

require_once(MODELS_WEBSITE_CLASS."database/query.inc.php");
require_once(MODELS_WEBSITE_CLASS."database/database.inc.php");

class Manager
{
    # hydrate

    public static function hydrate($oObject, $aArray)
    {
        $sManager = self::getManagerByClass($oObject);
        $sClass = get_class($oObject);
        foreach ($sManager::$aStructure as $sAttname => $aStruct) {
            $sMethod = 'set'.ucfirst(substr($sAttname, 1));
            if (method_exists($sClass, $sMethod)) {
                if (isset($aArray[$aStruct['attname']])) {
                    $oObject->$sMethod($aArray[$aStruct['attname']]);
                }
            }
        }
    }

    # get

    private static function getManagerByClass($oObject)
    {
        $sManager = get_class($oObject).'_Manager';
        if (!class_exists($sManager)) {
            trigger_error($sManager.' doesn\'t exist.');
        }

        return $sManager;
    }

    private static function getClassByManager($sClass)
    {
        $sClass = str_replace('_Manager', '', $sClass);
        if (!class_exists($sClass)) {
            trigger_error($sClass.' doesn\'t exist.');
        }

        return $sClass;
    }

    private static function getManager($sManager)
    {
        $sManager = $sManager."_Manager";
        if (!class_exists($sManager)) {
            trigger_error($sManager.' doesn\'t exist.');
        }

        return $sManager;
    }

    private static function getClass($sClass)
    {
        if (!class_exists($sClass)) {
            trigger_error($sClass.' doesn\'t exist.');
        }

        return $sClass;
    }

    # queries

    public static function insert($oDb, $oObject)
    {
        $sManager = self::getManagerByClass($oObject);
        $oQuery = new Query($sManager::$sTable, $oDb);
        $oQuery->setType("INSERT");

        foreach ($sManager::$aStructure as $sAttname => $aStruct) {
            $sMethod = 'get'.ucfirst(substr($sAttname, 1));
            if ($aStruct['default']) {
                $oQuery->setSets($aStruct[$sAttname], $aStruct['value']);
            } else {
                $oQuery->setSets($aStruct[$sAttname], $oObject->$sMethod());
            }
        }

        return $oQuery->exec();
    }

    public static function update($oDb, $oObject, $aParams)
    {
        $sManager = self::getManagerByClass($oObject);
        $oQuery = new Query($sManager::$sTable, $oDb);
        $oQuery->setType("UPDATE");

        foreach ($sManager::$aStructure as $sAttname => $aStruct) {
            $sMethod = 'get'.ucfirst(substr($sAttname, 1));
            $oQuery->setSets($aStruct[$sAttname], $oObject->$sMethod());
        }

        foreach ($aParams['where'] as $sAttname => $aStruct) {
            $oQuery->addCondition($sManager::$aStructure[$sAttname]["attname"], $aContent['operation'], $aContent['value']);
        }

        if (isset($aParams['order'])) {
            $oQuery->setOrder($aParams['order']);
        }

        if (isset($aParams['quantity'])) {
            $oQuery->setQuantity($aParams['quantity']);
        }

        return $oQuery->exec();
    }

    public static function delete($oDb, $sClass, $aParams)
    {
        $sManager = self::getManager($sClass);
        $oQuery = new Query($sManager::$sTable, $oDb);
        $oQuery->setType("DELETE");

        //todo : there is an issue here bitch
        foreach ($aParams['where'] as $sAttname => $aStruct) {
            $oQuery->addCondition($sManager::$aStructure[$sAttname]["attname"], $aContent['operation'], $aContent['value']);
        }

        if (isset($aParams['order'])) {
            $oQuery->setOrder($aParams['order']);
        }

        if (isset($aParams['quantity'])) {
            $oQuery->setQuantity($aParams['quantity']);
        }

        return $oQuery->exec();
    }

    public static function query($oDb, $sQuery, $aOptions){
        return $oDb->exec($sQuery, $aOptions);
    }

    public static function select($oDb, $sClass, $aParams)
    {
        $sManager = self::getManager($sClass);
        $oQuery = new Query($sManager::$sTable, $oDb);
        $oQuery->setType("SELECT");

        if(isset($aParams['where'])){
            foreach ($aParams['where'] as $sAttname => $aContent) {
                $oQuery->addCondition($sManager::$aStructure[$sAttname]["attname"], $aContent['operation'], $aContent['value']);
            }
        }

        if (isset($aParams['order'])) {
            $oQuery->setOrder($aParams['order']);
        }

        if (isset($aParams['asc'])) {
            $oQuery->setOrderAsc($aParams['asc']);
        }

        if (isset($aParams['offset'])) {
            $oQuery->setOffset($aParams['offset']);
        }

        if (isset($aParams['quantity'])) {
            $oQuery->setQuantity($aParams['quantity']);
        }

        $aResults = $oQuery->exec();

        //echo "<pre>".print_r($aResults, true)."</pre>";

        if (empty($aResults[0])) {
            return false;
        }

        $aObjects = array();
        foreach ($aResults as $aResult) {
            $aObjects[] = new $sClass(false);
            self::hydrate($aObjects[count($aObjects)-1], $aResult);
        }

        if (count($aObjects) == 1) {
            return $aObjects[0];
        } else if (!empty($aObjects)) {
            return $aObjects;
        } else {
            return false;
        }
    }

    public static function count($oDb, $sClass, $aParams)
    {
        $sManager = self::getManager($sClass);
        $oQuery = new Query($sManager::$sTable, $oDb);
        $oQuery->setType("COUNT");

        foreach ($aParams['where'] as $sAttname => $aContent) {
            $oQuery->addCondition($sManager::$aStructure[$sAttname]["attname"], $aContent['operation'], $aContent['value']);
        }

        $aResult = $oQuery->exec();

        if (!empty($aResult)) {
            return $aResult;
        }//else
        return 0;
    }
}
