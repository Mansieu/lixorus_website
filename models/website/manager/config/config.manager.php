<?php

require_once(MODELS_WEBSITE_MANAGER."/manager.inc.php");
require_once(MODELS_WEBSITE_CLASS."/config/config.class.php");

class Config_Manager extends Manager
{
    public static $sClass = "Config";
    public static $sTable = "config";
    public static $sPrimary = "name";

    public static $aStructure = array(
        'sName' =>           array('attname' => 'name',         'type' => 'varchar',    'lenght' => '255'),
        'sTheme' =>          array('attname' => 'theme',        'type' => 'varchar',    'lenght' => '255'),
        'bMaintenance' =>    array('attname' => 'maintenance',  'type' => 'int',        'lenght' => '1'),
    );

    public static function get($oDb, $sValue = "")
    {
        $aParams = array(
            "offset" => 0,
            "quantity" => 1,
        );
        $result = self::select($oDb, self::$sClass, $aParams);
        return $result;
    }

    public static function set(){
        //
    }
}
