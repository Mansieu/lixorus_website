<?php

require_once(MODELS_WEBSITE_MANAGER."manager.inc.php");
require_once(MODELS_WEBSITE_CLASS."news/news.class.php");

class News_Manager extends Manager
{
    public static $sClass = "News";
    public static $sTable = "news";
    public static $sPrimary = "language, id";

    public static $aStructure = array(
        "sId"           => array("attname" => "id",          "default" => false,      "value" => ""),
        "sTitle"        => array("attname" => "title",       "default" => false,      "value" => ""),
        "sSummary"      => array("attname" => "summary",     "default" => false,      "value" => ""),
        "sContent"      => array("attname" => "content",     "default" => false,      "value" => ""),
        "sDate"         => array("attname" => "date",        "default" => false,      "value" => ""),
        "iAuthor"       => array("attname" => "author",      "default" => false,      "value" => ""),
        "iLanguage"     => array("attname" => "language",    "default" => false,      "value" => ""),
        "sImage"        => array("attname" => "image",       "default" => false,      "value" => ""),
    );

    public static function get($oDb)
    {
        $aParams = array(
            "offset" => 0,
            "quantity" => 9,
            "order" => self::$sPrimary,
            "asc" => false,
        );

        return self::select($oDb, self::$sClass, $aParams);
    }
}
