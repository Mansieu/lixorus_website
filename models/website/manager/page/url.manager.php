<?php

require_once(MODELS_WEBSITE_MANAGER."manager.inc.php");
require_once(MODELS_WEBSITE_CLASS."page/url.class.php");

class Url_Manager extends Manager
{
    public static $sClass = "Url";
    public static $sTable = "uri";
    public static $sPrimary = "rule";

    public static $aStructure = array(
        "sRule"      => array("attname" => "rule",       "default" => false,     "value" => ""),
        "sAction"    => array("attname" => "action",     "default" => false,     "value" => ""),
        "sApp"       => array("attname" => "location",   "default" => false,     "value" => ""),
        "sLang"      => array("attname" => "lang",       "default" => false,     "value" => ""),
    );

    public static function getCurrent($oDb, $sValue = "")
    {
        $aParams = array(
            "where" => array(
                "sRule" => array(
                    "operation" => "REGEXP",
                    "value" => $sValue,
                ),
            ),
            "offset" => 0,
            "quantity" => 1,
        );
        return self::select($oDb, self::$sClass, $aParams);
    }
}
