<?php

class Pagination{
    private $sContent = null;
    private $aContent = null;

    private $sTheme = null;
    private $sApp = null;

    const XML_PATH = "part/paging.php";

    public function __construct($sTheme, $sApp)
    {
        $this->sTheme = $sTheme;
        $this->sApp = $sApp;
    }

    public function build()
    {
        if ($sFilePath = $this->getFormat()) {
            include($sFilePath);
            //echo "<pre>".print_r($aPagination, true)."</pre>";
        }
    }

    # set

    public function setTheme($sTheme)
    {
        $this->sTheme = $sTheme;
    }

    public function setApp($sApp)
    {
        $this->sApp = $sApp;
    }

    # get

    private function getFormat()
    {
        $sFilePath = $this->getFilePath();
        if (file_exists($sFilePath)) {
            return $sFilePath;
        }//else
        return false;
    }

    private function getFilePath(){
        return TEMPLATES.$this->sTheme."/".$this->sApp."/".self::XML_PATH;
    }
}

?>
