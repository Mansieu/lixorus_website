<?php

class Input{
    private $sLang = null;
    private $sName = null;
    private $sType = null;
    private $sPlaceholder = null;
    private $bRequired = null;

    public function __construct($sLang, $sName, $sType, $sPlaceholder, $bRequired){
        $this->sLang = $sLang;
        $this->sName = $sName;
        $this->sType = $sType;
        $this->sPlaceholder = $sPlaceholder;
        $this->bRequired = $bRequired;
    }

    public function getLang(){
        return $this->sLang;
    }

    public function getName(){
        return $this->sName;
    }

    public function getType(){
        return $this->sType;
    }

    public function getPlaceholder(){
        return $this->sPlaceholder;
    }

    public function getRequired(){
        return $this->bRequired;
    }
}

?>
