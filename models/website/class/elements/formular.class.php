<?php

require_once(MODELS_WEBSITE_CLASS."elements/input.class.php");

class Model_Formular{
    private $sLocation = null;
    private $sMethod = null;

    private $aInputs = null;
    private $aRows = null;

    public function __construct($sLocation, $sMethod){
        $this->sLocation = $sLocation;
        $this->sMethod = $sMethod;
    }

    public function addInputs($aInputs){
        foreach($aInputs as $aInput){
            if(isset($aInput['row']) && !empty($aInput['row'])){
                if(isset($aInput['size']) && !empty($aInput['size'])){
                    $this->addInput($aInput['lang'], $aInput['name'], $aInput['placeholder'], $aInput['required'], $aInput['row'], $aInput['size']);
                }else{
                    $this->addInput($aInput['lang'], $aInput['name'], $aInput['placeholder'], $aInput['required'], $aInput['row']);
                }
            }else{
                $this->addInput($aInput['lang'], $aInput['name'], $aInput['placeholder'], $aInput['required']);
            }
        }
    }

    public function addInput($sLang, $sName, $sType, $sPlaceholder, $bRequired, $sRow = "", $sCol = ""){
        $this->aInputs[$sRow][$sCol][] = new Input($sLang, $sName, $sType, $sPlaceholder, $bRequired);
    }
}

?>
