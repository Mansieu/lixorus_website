<?php

require_once(MODELS_WEBSITE_CLASS."config/utils.inc.php");
require_once(MODELS_WEBSITE_CLASS."dictionnary/dictionnary.inc.php");
require_once(MODELS_WEBSITE_CLASS."page/url.class.php");
require_once(MODELS_WEBSITE_MANAGER."config/config.manager.php");
require_once(MODELS_WEBSITE_MANAGER."page/url.manager.php");

class Page
{
    private $sTitle = "";
    private $sContent = "";

    //private $oUrl = null;
    private $oConfig = null;

    private $iLanguage = LANG_DEFAULT;

    private $oDatabaseWebsite = null;
    private $oDatabaseGame = null;

    private $aOutputs = array();

    public function __construct($oDatabaseWebsite/*, $oDatabaseGame*/, $sTitle = "")
    {
        $this->setDb("w", $oDatabaseWebsite);
        //$this->setDb("g", $oDatabaseGame);

        $this->oConfig = Config_Manager::get($this->getDb("w"));

        // move it to the manager/class ?
        /*
        $this->oUrl = Url_Manager::getCurrent($this->getDb("w"), Url::getUri());
        if (!$this->oUrl || !$this->oUrl->check("view")) {
            echo "<!-- 404 error -->";
            $this->oUrl = new Url();
            $this->oUrl->fill("website", "404", "File not found");
        }
        $this->oUrl->setTheme($this->oConfig->getTheme());
        */

        //Dict::build($this->getDb("w"), $this->iLanguage, $this->oUrl->getApp(), $this->oUrl->getAction());
        //$this->setLanguage();

        $this->setTitle("Lixorus");

        /*
        if (!empty($sTitle)) {
            $this->setTitle($sTitle);
        } elseif ($sTitle == Dict::get("base:title")) {
            $this->setTitle($sTitle);
        } else {
            $this->setTitle($this->oUrl->getLang());
        }
        */
    }

    # outputs

    public function addOutput($oOutput){
        if(!empty($oOutput)){
            $this->aOutputs[] = $oOutput;
            return true;
        }
        return false;
    }

    public function hasOutputs(){
        if(count($this->aOutputs) > 0){
            return true;
        }
        return false;
    }

    public function getOutputs(){
        if($this->hasOutputs()){
            return $this->aOutputs;
        }
        return false;
    }

    # get

    public function template($sApp = "website")
    {
        return URL."/".TEMPLATES."default/".$sApp."/";
    }

    public function get($sType, $sParam = "")
    {
        switch ($sType) {
            case "basic": // todo
                return APPROOT."/".APPS."website/views/".$sType."/".$sParam.".view.php";
            default:
                //return $this->oUrl->get($sType, $sParam);
                return "";
        }
    }

    public function getDb($sType)
    {
        switch($sType){
            case "website":
            case "w":
                return $this->oDatabaseWebsite;
            case "game":
            case "g":
                return $this->oDatabaseGame;
        }
    }

    public function getTitle()
    {
        return (empty($this->sTitle)) ? $this->oConfig->getName() : $this->oConfig->getName()." - ".$this->sTitle;
    }

    public function getUrl()
    {
        return $this->oUrl;
    }

    public function getConfig()
    {
        return $this->oConfig;
    }

    # set

    public function setDb($sType, $oDatabase)
    {
        switch($sType){
            case "website":
            case "w":
                $this->oDatabaseWebsite = $oDatabase;
                return true;
            case "game":
            case "g":
                $this->oDatabaseGame = $oDatabase;
                return true;
        }
        return false;
    }

    public function setTitle($sTitle)
    {
        if (!empty($sTitle)) {
            $this->sTitle = $sTitle;
        }
    }

    public function setLanguage($iLanguage = "")
    {
        if (!empty($iLanguage)) {
            $this->iLanguage = $iLanguage;
        } else {
            $this->iLanguage = LANG_DEFAULT;
        }
        //Dict::build($this->getDb("w"), $this->iLanguage, $this->oUrl->getApp(), $this->oUrl->getAction());
        //$this->setTitle(Dict::get("base:title"));
        $this->setTitle("Lixorus");
    }

    public function getLanguage()
    {
        return $this->iLanguage;
    }
}
