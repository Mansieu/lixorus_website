<?php

require_once(MODELS_WEBSITE_CLASS."config/utils.inc.php");

class Url
{
    # variable

    private $sBase = null;

    private $sTheme = THEME_DEFAULT;

    private $sApp = null;
    private $sAction = null;
    private $sLang = null;

    private $sModule = null;

    private $sUrlBase = null;

    # function

    public function __construct($sTheme = "")
    {
        $this->sTheme = $sTheme;
        $this->sBase = APPROOT;
        $this->setBase();
    }

    public function fill($sApp, $sAction, $sLang){
        $this->setApp($sApp);
        $this->setAction($sAction);
        $this->setLang($sLang);
    }

    public static function redirect($sUrl)
    {
        //$string = '<script type="text/javascript">';
        //$string .= 'window.location = "' . $sUrl . '"';
        //$string .= '</script>';

        //echo $string;

        header("Location: ".$sUrl);
        exit();
    }

    public function check($sType = "view")
    {
        $sFilePath = $this->get($sType);
        if (file_exists($sFilePath)) {
            return true;
        }
        return false;
    }

    # url building

    public function get($sType, $sName = "")
    {
        switch ($sType) {
            case "template":
                $sFilePath = self::sget($sType, $this->sApp, $this->sUrlBase, $this->sAction, $sName, $this->sTheme);
                if (file_exists($sFilePath)) {
                    return $sFilePath;
                }
                return false;
            default:
                $sFilePath = self::sget($sType, $this->sApp, $this->sBase, $this->sAction, $sName);
                if (file_exists($sFilePath)) {
                    return $sFilePath;
                }
                return false;
        }
    }

    public static function sget($sType, $sApp, $sBase, $sAction, $sName = "", $sTheme = "")
    {

        switch ($sType) {
            case "layout":
                if (!empty($sApp)) {
                    $sFilePath = $sBase."/".APPS.$sApp."/".self::parseByType($sType, $sAction);
                    if(file_exists($sFilePath)){
                        return $sFilePath;
                    }
                    return "";
                }
                return "";
            case "view":
                if (!empty($sApp)) {
                    $sFilePath = $sBase."/".APPS.$sApp."/".VIEWS.self::parseByType($sType, $sAction);
                    if(file_exists($sFilePath)){
                        return $sFilePath;
                    }
                    return "";
                }
                return "";
            case "basic":
                if (!empty($sApp)) {
                    return $sBase."/".APPS.$sApp."/".VIEWS."basic/".$sName.".view".PHP;
                }
                return "";
            case "footer":
                if (!empty($sApp)) {
                    return $sBase."/".APPS.$sApp."/".VIEWS."basic/footer.view".PHP;
                }
                return "";
            case "widget":
                if (!empty($sApp)) {
                    return $sBase."/".APPS.$sApp."/".VIEWS."widget/".$sName.".view".PHP;
                }
                return "";
            case "controller":
                if (!empty($sApp)) {
                    return $sBase."/".APPS.$sApp."/".CONTROLLERS.self::parseByType($sType, $sAction);
                }
                return "";
            case "dict":
                if (!empty($sApp)) {
                    return $sBase."/".APPS.$sApp."/".DICTS.self::parseByType($sType, $sAction);
                }
                return "";
            case "script":
                if (!empty($sApp)) {
                    return $sBase."/".APPS.$sApp."/".SCRIPTS.self::parseByType($sType, $sAction);
                }
                return "";
            case "template":
                if (!empty($sApp)) {
                    echo $sBase."/".TEMPLATES.$sTheme."/".$sApp."/";
                }
                return "";
            default:
                return "";
        }
    }

    # parse

    public static function parseByType($sType, $sAction)
    {
        return $sAction.".".$sType.PHP;
    }

    # set

    public function setAction($sItem)
    {
        $this->sAction = $sItem;
    }

    public function setLang($sItem)
    {
        $this->sLang = $sItem;
    }

    public function setApp($sItem)
    {
        $this->sApp = $sItem;
    }

    public function setModule($sItem)
    {
        $this->sModule = $sItem;
    }

    public function setTheme($sTheme)
    {
        if(!empty($sTheme)){
            $this->sTheme = $sTheme;
        }
    }

    public function setBase()
    {
        $this->sUrlBase = "http://".$_SERVER['SERVER_NAME'];
    }

    # get

    public function getBase()
    {
        return $this->sUrlBase;
    }

    public function getAction()
    {
        return $this->sAction;
    }

    public function getLang()
    {
        return $this->sLang;
    }

    public function getApp()
    {
        return $this->sApp;
    }

    public function getModule()
    {
        return $this->sModule;
    }

    public static function getUri()
    {
        return $_SERVER['REQUEST_URI'];
    }
}
