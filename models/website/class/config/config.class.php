<?php

class Config
{
    # variable

    private $sName = null;
    private $sTheme = null;
    private $bMaintenance = false;

    # function

    public function __construct($sTheme)
    {
        $this->setTheme($sTheme);
    }

    # set

    public function setTheme($sTheme)
    {
        if (!empty($sTheme)) {
            if (Utils::isDirectory(TEMPLATES.$sTheme)) {
                $this->sTheme = $sTheme;
            } else {
                $this->sTheme = THEME_DEFAULT;
            }
        } else {
            $this->sTheme = THEME_DEFAULT;
        }
    }

    public function setMaintenance($bMaintenance)
    {
        if (is_bool($bMaintenance)) {
            $this->bMaintenance = $bMaintenance;
        }
    }

    public function setName($sName)
    {
        if (!empty($sName)) {
            $this->sName = $sName;
        }
    }

    # get

    public function getTheme()
    {
        return $this->sTheme;
    }

    public function getName()
    {
        return $this->sName;
    }
}
