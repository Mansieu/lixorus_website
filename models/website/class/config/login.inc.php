<?php

require_once(MODELS_WEBSITE_MANAGER."account/account.manager.php");

class Login
{
    public static $iAccount = null;
    public static $oAccount = null;
    public static $bConnected = false;

    public static function connect($aParams)
    {
        //self::setConnected($oParams);

        // if $_SESSION['connected'] has not been set as of yet
        if (!isset($_SESSION['connected'])) {
            //if an user is already connected
            if ($_SESSION['connected'] === true) {
                //if the connected user is different than the user trying to log in
                if (isset($aParams['id']) && $aParams['id'] != self::$iAccount) {
                    $_SESSION['account'] = $aParams['id'];
                    $_SESSION['connected'] = true;
                }
            } else { // if not already connected
                if (isset($aParams['id'])) {
                    $_SESSION['account'] = $aParams['id'];
                    $_SESSION['connected'] = true;
                }
            }
        } else {
            //
        }
    }

    public static function disconnect()
    {
        $_SESSION['connected'] = false;
        $_SESSION['account'] = null;
    }

    public static function isConnected()
    {
        return self::$bConnected;
    }

    public static function setConnected($oParams = array())
    {
        //if there is an id but it's not numeric, return
        if (isset($oParams['id']) && !is_numeric($oParams['id'])) {
            return;
        }
        //connects if is disconnected, disconnects if is connected
        if (isset($_SESSION['connected'])) {
            if ($_SESSION['connected'] === false) {
                self::$iAccount = null;
                self::$bConnected = false;
            } elseif ($_SESSION['connected'] === true) {
                self::$iAccount = $oParams['id'];
                self::$bConnected = true;
            }
        }
    }
}
