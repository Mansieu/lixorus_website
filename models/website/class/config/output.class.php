<?php

abstract class Output
{
    protected $sLang;
    protected $sTitle;
    protected $sType;

    public function __construct()
    {
    }

    public function write()
    {
        return '<div class="alert alert-'.$this->sType.'"> <strong>'.$this->sTitle.'</strong> - '.$this->sLang.'</div>';
    }

    public function __call($method, $args)
    {
        $variable = substr($method, 3);

        if (isset($this->$variable)) {
            if (strncasecmp($method, 'get', 3) == 0) {
                return $this->$variable;
            }
        }

        if (strncasecmp($method, 'set', 3) == 0) {
            $this->$variable = $args[0];
        }
    }
}

class Danger extends Output
{
    public function __construct($sTitle = "Danger", $sLang = "votre utilisation particulière de ces carottes n'est pas autorisée")
    {
        $this->sLang = $sLang;
        $this->sTitle = $sTitle;
        $this->sType = "danger";
    }
}

class Warning extends Output
{
    public function __construct($sTitle = "Attention", $sLang = "si j'étais vous, j'n'utiliserais pas ces carottes comme ca")
    {
        $this->sLang = $sLang;
        $this->sTitle = $sTitle;
        $this->sType = "warning";
    }
}

class Success extends Output
{
    public function __construct($sTitle = "Bravo", $sLang = "vos fesses sont plus roses")
    {
        $this->sLang = $sLang;
        $this->sTitle = $sTitle;
        $this->sType = "success";
    }
}

class Information extends Output
{
    public function __construct($sTitle = "Information", $sLang = "les carottes rendent les fesses roses")
    {
        $this->sLang = $sLang;
        $this->sTitle = $sTitle;
        $this->sType = "info";
    }
}
