<?php

class Utils
{
    # get POST and GET variables

    public static function get($sName)
    {
        if (isset($_GET[$sName]) && !empty($_GET[$sName])) {
            return $_GET[$sName];
        }
        return false;
    }

    public static function post($sName)
    {
        if (isset($_POST[$sName]) && !empty($_POST[$sName])) {
            return $_POST[$sName];
        }
        return false;
    }

    public static function read($sName)
    {
        if ($sResult = self::post($sName)) {
            return $sResult;
        }
        if ($sResult = self::get($sName)) {
            return $sResult;
        }
        return false;
    }

    # verify directory or file existance

    public static function isFile($sPath)
    {
        if (file_exists($sPath) && is_dir($sPath)) { // is directory
            return false;
        }
        if (file_exists($sPath)) { // is file
            return true;
        }
        return false;
    }

    public static function isDirectory($sPath)
    {
        if (file_exists($sPath) && is_dir($sPath)) { // is directory
            return true;
        }
        return false;
    }

    public static function setCookie($aParams){
        if(!isset($aParams['name']) || empty($aParams['name'])){
            return false;
        }
        if(!isset($aParams['value']) || empty($aParams['value'])){
            return false;
        }
        if(isset($aParams['duration']) && !empty($aParams['duration']) && ($aParams['duration'] > 0)){
            setcookie($aParams['name'], $aParams['value'], $aParams['duration']);
        }else{
            setcookie($aParams['name'], $aParams['value'], time() + (10 * 365 * 24 * 60 * 60));
        }
    }

    # build links

    public static function url($aParams, $sElement)
    {
        switch (strtolower($aParams['type'])) {
            case "css":
                if (isset($aParams['theme']) and !empty($aParams['theme'])) {
                    return TEMPLATES.$aParams['theme']."/css/".$sElement;
                }
                return false;
            case "js":
                if (isset($aParams['theme']) and !empty($aParams['theme'])) {
                    return TEMPLATES.$aParams['theme']."/js/".$sElement;
                }
                return false;
            default:
                return false;
        }
    }
}
