<?php

require_once(MODELS_WEBSITE_MANAGER."dictionnary/term.manager.php");

class Dict{
    private static $aTerms = array();
    private static $oDb = null;
    private static $iLanguage = null;
    private static $sApp = null;
    private static $sAction = null;

    public static function build($oDb, $iLanguage, $sApp, $sAction)
    {
        self::$aTerms = array();
        self::$oDb = $oDb;
        self::$iLanguage = $iLanguage;
        self::$sApp = $sApp;
        self::$sAction = $sAction;

        self::load();
    }

    public static function load()
    {
        self::loadAction(self::$iLanguage, self::$sApp, "base");
        self::loadAction(self::$iLanguage, self::$sApp, "menu");
        self::loadAction(self::$iLanguage, self::$sApp, "footer");
        self::loadAction(self::$iLanguage, self::$sApp, self::$sAction);
    }

    public static function loadAction($iLanguage, $sApp, $sAction)
    {
        $aResult = Term_Manager::getCurrent(self::$oDb, $iLanguage, $sApp, $sAction);
        if ($aResult) {
            if (is_array($aResult)) {
                foreach ($aResult as $sKey => $oTerm) {
                    self::$aTerms[$oTerm->getLanguage()][$oTerm->getIndex()] = $oTerm;
                }
            } else {
                self::$aTerms[$aResult->getLanguage()][$aResult->getIndex()] = $aResult;
            }
        }
    }

    public static function add($iLanguage, $aTerms)
    {
        foreach ($aTerms as $sKey => $sValue) {
            self::$aTerms[$iLanguage][$sKey] = $sValue;
        }
    }

    public static function get($sKey)
    {
        if(isset(self::$aTerms[self::$iLanguage][$sKey]) && !empty(self::$aTerms[self::$iLanguage][$sKey])){
            return self::$aTerms[self::$iLanguage][$sKey]->getValue();
        }
        return $sKey;
    }

    public static function self()
    {
        return array(self::$aTerms, self::$iLanguage, self::$sApp, self::$sAction);
    }
}

?>
