<?php

class Term{
    private $iLanguage = null;
    private $sAction = null;
    private $sIndex = null;
    private $sValue = null;
    private $sApp = null;

    public function __construct()
    {
        //
    }

    # set

    public function setAction($sValue)
    {
        $this->sAction = $sValue;
    }

    public function setLanguage($iValue)
    {
        $this->iLanguage = $iValue;
    }

    public function setIndex($sValue)
    {
        $this->sIndex = $sValue;
    }

    public function setValue($sValue)
    {
        $this->sValue = $sValue;
    }

    public function setApp($sValue)
    {
        $this->sApp = $sValue;
    }

    # get

    public function getAction()
    {
        return $this->sAction;
    }

    public function getLanguage()
    {
        return $this->iLanguage;
    }

    public function getIndex()
    {
        return $this->sIndex;
    }

    public function getValue()
    {
        return $this->sValue;
    }

    public function getApp()
    {
        return $this->sApp;
    }

    # tostring

    public function __toString()
    {
        return $this->sLang;
    }
}

?>
