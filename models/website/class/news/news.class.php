<?php

class News{
    private $iId = null;

    private $sTitle = null;
    private $sSummary = null;
    private $sContent = null;

    private $sDate = null;
    private $iAuthor = null;
    private $iLanguage = null;

    private $sImage = null;

    public function __construct(){
        //
    }

    # parse

    # set

    public function setId($sValue){
        $this->iId = $sValue;
    }

    public function setTitle($sValue){
        $this->sTitle = $sValue;
    }

    public function setSummary($sValue){
        $this->sSummary = $sValue;
    }

    public function setContent($sValue){
        $this->sContent = $sValue;
    }

    public function setDate($sValue){
        $this->sDate = $sValue;
    }

    public function setAuthor($iValue){
        $this->iAuthor = $iValue;
    }

    public function setLanguage($iValue){
        $this->iLanguage = $iValue;
    }

    public function setImage($sValue){
        $this->sImage = $sValue;
    }

    # get

    public function getId(){
        return $this->iId;
    }

    public function getTitle(){
        return $this->sTitle;
    }

    public function getSummary(){
        return $this->sSummary;
    }

    public function getContent(){
        return $this->sContent;
    }

    public function getDate(){
        return explode(" ", $this->sDate)[0];
    }

    public function getAuthor(){
        return $this->iAuthor;
    }

    public function getLanguage(){
        return $this->iLanguage;
    }

    public function getImage(){
        if(file_exists(RESSOURCES."news/".$this->iId.".png")){
            return $this->iId.".png";
        }//else
        if(file_exists(RESSOURCES."news/".$this->iId.".jpg")){
            return $this->iId.".jpg";
        }//else
        return "default.png";
    }
}

?>
