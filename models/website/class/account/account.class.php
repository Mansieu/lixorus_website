<?php

require_once(MODELS_WEBSITE_MANAGER."account/account.manager.php");

class Account{
    private $iId = null;
    private $sUserName = null;
    private $sPassword = null;
    private $sSalt = null;
    private $sEmail = null;
    private $sKey = null;
    private $iRank = null;

    public function __construct(){
        //
    }

    private function generateKey($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function register(){

    }

    # add

    public function addUserName($sValue){
        if(empty($sValue)){
            return new Error();
        }
    }

    # set

    public function setId($iValue){
        $this->iId = $iValue;
    }

    public function setUserName($sValue){
        $this->sUserName = $sValue;
    }

    public function setPassword($sValue){
        $this->sPassword = $sValue;
    }

    public function setSalt($sValue){
        $this->sSalt = $sValue;
    }

    public function setEmail($sValue){
        $this->sEmail = $sValue;
    }

    public function setKey($sValue){
        $this->sKey = $sValue;
    }

    public function setRank($iValue){
        $this->iRank = $iValue;
    }

    # get

    public function getId(){
        return $this->iId;
    }

    public function getUserName(){
        return $this->sUserName;
    }

    public function getPassword(){
        return $this->sPassword;
    }

    public function getSalt(){
        return $this->sSalt;
    }

    public function getEmail(){
        return $this->sEmail;
    }

    public function getKey(){
        return $this->sKey;
    }

    public function getRank(){
        return $this->iRank;
    }
}

?>
