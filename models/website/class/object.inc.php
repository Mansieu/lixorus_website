<?php

/**
 * this is the base class for every class : it is no longer necessary to write for each a __construct if
 * there is no precise need for it
 */

class Object
{
    private $id = null;
    private $new = null;

    /**
     * constructor
     * @param boolean $new [if item is new or already existing]
     */
    public function __construct($new = true)
    {
        $this->New = $new;
    }

    public function save()
    {
        if ($this->new) {
            $this->insert();
        } else {
            $this->update();
        }
    }
}
