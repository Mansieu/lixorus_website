<?php

class Database
{
    private $oDatabase = null;
    private $sName = null;

    private $bConnected = false;

    public function __construct($sHost, $sName, $sUser, $sPass)
    {
        $this->sName = $sName;
        $this->connect($sHost, $sName, $sUser, $sPass);
    }

    public function connect($sHost, $sName, $sUser, $sPass, $sPort = "3306")
    {
        try {
            $this->oDatabase = new PDO("mysql:host=$sHost;port=$sPort;dbname=$sName", $sUser, $sPass);
            $this->bConnected = true;
            return true;
        } catch (PDOException $e) {
            echo "Failed to get DB handle: " . $e->getMessage() . "<br />\n";
        }
        return false;
    }

    public function query($sQuery)
    {
        if ($this->bConnected) {
            if (empty($sQuery)) {
                return false;
            }
            return $this->oDatabase->query($sQuery);
        } else {
            return false;
        }
    }

    public function exec($sQuery, $aOptions = null)
    {
        if ($this->bConnected) {
            if (empty($sQuery)) {
                return false;
            }

            $oStatement = $this->oDatabase->prepare($sQuery);
            if (empty($aOptions)) {
                $oResult = $oStatement->execute();
                if(!$oResult){
                    var_dump($oStatement->errorInfo());
                }
            } else {
                $oResult = $oStatement->execute($aOptions);
                if(!$oResult){
                    var_dump($oStatement->errorInfo());
                }
            }

            $aResults = array();
            while ($aRow = $oStatement->fetch()) {
                $aResults[] = $aRow;
            }

            if ($oResult) {
                return $aResults;
            } else {
                return $oResult;
            }
        } else {
            return false;
        }
    }

    public function rowCount($sQuery, $aOptions = array())
    {
        if ($this->bConnected) {
            if (!empty($aOptions)) {
                $oStatement = $this->oDatabase->prepare($sQuery);
                $oStatement->execute($aOptions);
            } else {
                $oStatement = $this->oDatabase->prepare($sQuery);
                $oStatement->execute();
            }
            $sCount = $oStatement->rowCount();
            return $sCount;
        } else {
            return false;
        }
    }
}
