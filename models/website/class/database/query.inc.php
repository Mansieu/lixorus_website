<?php

class Query
{
    private $sTable = null;
    private $oDatabase = null;

    # query type (select, insert, update, delete)
    private $sType = null;

    # where
    private $aOptions = array();
    private $aConditions = array();

    # set
    private $aSets = array();

    # limit
    private $iOffset = null;
    private $iQuantity = null;

    # order by
    private $sOrder = null;
    private $bAsc = true;

    private $sQuery = null;

    public function __construct($sTable, $oDatabase)
    {
        $this->sTable = $sTable;
        $this->oDatabase = $oDatabase;
    }

    public function exec()
    {
        switch ($this->sType) {
            case "SELECT":
                $this->sQuery = $this->select();
                $aResult = $this->oDatabase->exec($this->sQuery, $this->aOptions);
                return $aResult;
            case "INSERT":
                $this->sQuery = $this->insert();
                $aResult = $this->oDatabase->exec($this->sQuery, $this->aOptions);
                return $aResult;
            case "UPDATE":
                $this->sQuery = $this->update();
                $aResult = $this->oDatabase->exec($this->sQuery, $this->aOptions);
                return $aResult;
            case "DELETE":
                $this->sQuery = $this->delete();
                $aResult = $this->oDatabase->exec($this->sQuery, $this->aOptions);
                return $aResult;
            case "COUNT":
                $this->sQuery = $this->count();
                $aResult = $this->oDatabase->rowCount($this->sQuery, $this->aOptions);
                return $aResult;
        }
    }

    # write query

    public function select()
    {
        return "SELECT * FROM `".$this->sTable."`".$this->parseWhere().$this->parseOrder().$this->parseLimit().";";
    }

    protected function insert()
    {
        return "INSERT INTO `".$this->sTable."`".$this->parseInsert().";";
    }

    protected function update()
    {
        return "UPDATE `".$this->sTable."`".$this->parseSet().$this->parseWhere().$this->parseLimit(false).";";
    }

    protected function delete()
    {
        return "DELETE FROM `".$this->sTable."`".$this->parseWhere().$this->parseLimit(false).";";
    }

    protected function count()
    {
        return "SELECT * FROM `".$this->sTable."`".$this->parseWhere();
    }

    # parse

    protected function parseInsert()
    {
        $aAttnames = "";
        $aValues = "";

        if (empty($this->aSets)) {
            return;
        }

        foreach ($this->aSets as $sAttname => $sValue) {
            $aAttnames[] = $sAttname;
            $aValues[] = $sValue;
        }

        return " ( ".implode(", ", $aAttnames)." ) VALUES ( ".implode(", ", $aValues)." )";
    }

    protected function parseSet()
    {
        $aResult = "";

        if (empty($this->aSets)) {
            return;
        }

        foreach ($this->aSets as $sAttname => $sValue) {
            $aResult[] = $sAttname." = ".$sValue;
        }

        return " SET ".implode(", ", $aResult);
    }

    protected function parseWhere()
    {
        $aResult = array();

        if (empty($this->aConditions)) {
            return;
        }

        foreach ($this->aConditions as $sAttname => $aCondition) {
            foreach ($aCondition as $sOperation => $sContent) {
                $aResult[] = $this->parseOperation($sOperation, $sContent, $sAttname);
            }
        }

        return " WHERE ".implode(" AND ", $aResult);
    }

    protected function parseOperation($sOperation, $sContent, $sAttname)
    {
        switch ($sOperation) {
            case "=":
            case "<>":
            case "<":
            case ">":
            case "<=":
            case ">=":
            case "LIKE":
            case "NOT":
            case "NOT LIKE":
                return $sAttname." ".$sOperation." ".$sContent;
            case "REGEXP":
            case "NOT REGEXP":
                return $sContent." ".$sOperation." ".$sAttname;
            default:
                return $sAttname." ".$sOperation."(".$sContent.")";
        }
    }

    protected function parseOrder()
    {
        if (empty($this->sOrder)) {
            return;
        }
        $sAsc = ($this->bAsc) ? "ASC" : "DESC";

        return " ORDER BY ".$this->sOrder." ".$sAsc;
    }

    protected function parseLimit($bSelect = true)
    {
        if ((empty($this->iOffset) && $this->iOffset != 0) || !is_numeric($this->iOffset)) {
            return;
        }
        if (empty($this->iQuantity) || !is_numeric($this->iQuantity)) {
            return;
        }

        if ($bSelect) {
            return " LIMIT ".$this->iOffset.",".$this->iQuantity;
        } else {
            return " LIMIT ".$this->iQuantity;
        }
    }

    # add

    public function addSet($sAttname, $sValue)
    {
        $this->aSets[$sAttname] = ":".$sAttname;
        $this->aOptions[":".$sAttname] = $sValue;
    }

    public function addCondition($sAttname, $sOperation, $sValue)
    {
        $iOps = (isset($this->aConditions[$sAttname][$sOperation])) ? count($this->aConditions[$sAttname]) : "" ;
        $this->aConditions[$sAttname][$sOperation] = ":".$sAttname.$iOps;
        $this->aOptions[":".$sAttname.$iOps] = $sValue;
    }

    # set

    public function setOrderAsc($bAsc = true)
    {
        $this->bAsc = $bAsc;
    }

    public function setOrder($sOrder)
    {
        if (!empty($sOrder)) {
            $this->sOrder = $sOrder;
        }
    }

    public function setOffset($iOffset)
    {
        if ((!empty($iOffset)  || $iOffset == 0) && is_numeric($iOffset)) {
            $this->iOffset = $iOffset;
        }
    }

    public function setQuantity($iQuantity)
    {
        if (!empty($iQuantity) && is_numeric($iQuantity)) {
            $this->iQuantity = $iQuantity;
        }
    }

    public function setType($sType)
    {
        $sType = strtoupper($sType);
        if (!empty($sType) && in_array($sType, array("SELECT", "INSERT", "UPDATE", "DELETE", "COUNT"))) {
            $this->sType = strtoupper($sType);
        }
    }
}
