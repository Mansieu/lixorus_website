<?php

require_once(MODELS_WEBSITE_MANAGER."manager.inc.php");
require_once(MODELS_SERVER_CLASS."map/map.class.php");

class Map_Manager extends Manager
{
    public static $sClass = "Map";
    public static $sTable = "maps";
    public static $sPrimary = "id";

    public static $aStructure = array(
        "iID"        => array("attname" => "id",       "default" => false,     "value" => ""),
        "iDate"      => array("attname" => "date",     "default" => false,     "value" => ""),
    );

    public static function getAll($oDb)
    {
        $aParams = array(
            "order" => "id",
            "offset" => 0,
            "quantity" => 10761,
            );
        return self::select($oDb, self::$sClass, $aParams);
    }
}
