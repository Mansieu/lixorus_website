<?php

class Map{
    private $iID = null;
    private $iDate = null;

    public function __construct()
    {
        //
    }

    # set

    public function setID($sValue)
    {
        $this->iID = $sValue;
    }

    public function setDate($iValue)
    {
        $this->iDate = $iValue;
    }

    # get

    public function getID()
    {
        return $this->iID;
    }

    public function getDate()
    {
        return $this->iDate;
    }

    # tostring

    public function __toString()
    {
        return $this->iID."_".$this->iDate."X.swf";
    }
}

?>
