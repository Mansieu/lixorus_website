<?php
/**
 * Created by PhpStorm.
 * User: Mansieu
 * Date: 23/02/2018
 * Time: 16:46
 */

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "website";

class dbObj
{
    public $con;
    public function getConnstring($servername, $username, $password, $dbname)
    {
        $con = mysqli_connect($servername, $username, $password, $dbname) or die("Connection failed: " . mysqli_connect_error());

        /* check connection */
        if (mysqli_connect_errno()) {
            printf("Connect failed: %s\n", mysqli_connect_error());
            exit();
        } else {
            $this->conn = $con;
        }
        return $this->conn;
    }
}

class Item
{
    protected $conn;
    protected $data = array();
    protected $table = "dictionnary";

    public function __construct($connString)
    {
        $this->conn = $connString;
    }

    public function get($params)
    {
        $this->data = $this->select($params);
        return json_encode($this->data);
    }

    public function select($params)
    {
        $rp = isset($params['rowCount']) ? $params['rowCount'] : 10;
        $rp = 10;

        if (isset($params['sort']) && is_array($params['sort']) )
        {
            $order_by=" ORDER BY";
            foreach($params['sort'] as $key=> $value){
                $order_by.=" $key $value";
            }
            if(empty($order_by)){
                $order_by = "aindex, language";
            }
        }

        if (isset($params['current'])) {
            $page  = $params['current'];
        } else {
            $page=1;
        };
        $start_from = ($page-1) * $rp;

        $sql = $sqlRec = $sqlTot = '';

        // getting total number records without any search
        $sql = "SELECT * FROM `".$this->table."`";
        $sqlTot .= $sql;
        $sqlRec .= $sql.$order_by;

        if ($rp!=-1) {
            $sqlRec .= " LIMIT ". $start_from .",".$rp;
        }

        $qtot = mysqli_query($this->conn, $sqlTot) or die("Une erreur est survenue lors de la récupération du nombre total de comptes");
        $queryRecords = mysqli_query($this->conn, $sqlRec) or die("Une erreur est survenue lors de la récupération des comptes");

        while ($row = mysqli_fetch_assoc($queryRecords)) {
            $data[] = $row;
        }

        $json_data = array(
            "current"             => intval($params['current']),
            "rowCount"            => 10,
            "total"               => intval($qtot->num_rows),
            "rows"                => $data   // total data array
        );

        return $json_data;
    }
}

if(empty($_REQUEST)){
    exit();
}

$db = new dbObj();
$connString =  $db->getConnstring($servername, $username, $password, $dbname);

$params = $_REQUEST;

$action = isset($params['action']) != '' ? $params['action'] : '';
$items = new Item($connString);

$result = "stuff";
switch ($action) {
    default:
        $result = $items->get($params);
        break;
}

echo $result;
return;
