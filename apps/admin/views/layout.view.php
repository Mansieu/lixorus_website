
<!DOCTYPE html>
<html lang="en">
<!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?php echo $oPage->template(); ?>img/egg-2.ico">

        <title><?php echo $oPage->getTitle(); ?></title>

        <!-- Vendor CSS -->
        <link href="<?php echo $oPage->template(); ?>vendors/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="<?php echo $oPage->template(); ?>vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="<?php echo $oPage->template(); ?>vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet">
        <link href="<?php echo $oPage->template(); ?>vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
        <link href="<?php echo $oPage->template(); ?>vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet">
        <link href="<?php echo $oPage->template(); ?>vendors/bootgrid/jquery.bootgrid.min.css" rel="stylesheet">
        <link href="<?php echo $oPage->template(); ?>vendors/summernote/dist/summernote.css" rel="stylesheet">
        <link href="<?php echo $oPage->template(); ?>vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">

        <!-- CSS -->
        <link href="<?php echo $oPage->template(); ?>css/app_1.min.css" rel="stylesheet">
        <link href="<?php echo $oPage->template(); ?>css/app_2.min.css" rel="stylesheet">
        <link href="<?php echo $oPage->template(); ?>css/custom.css" rel="stylesheet">


        <!-- JS -->
        <script src="<?php echo $oPage->template(); ?>vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="<?php echo $oPage->template(); ?>vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    </head>
    <body>
        <?php
        if ($sFilePath = $oPage->get("basic", "header")) {
            include($sFilePath);
        }
        ?>
        <section id="main">
            <?php
            if ($sFilePath = $oPage->get("basic", "menu")) {
                include($sFilePath);
            }
            ?>
            <section id="content">
                <div class="container">
                <?php
                if ($sFilePath = $oPage->get("view")) {
                    include($sFilePath);
                }
                ?>
                </div>
            </section>
            <?php
            if ($sFilePath = $oPage->get("basic", "footer")) {
                include($sFilePath);
            }
            ?>
        </section>
    </body>
</html>
