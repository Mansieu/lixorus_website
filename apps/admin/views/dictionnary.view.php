<?php
/**
 * Created by PhpStorm.
 * User: Mansieu
 * Date: 04/02/2018
 * Time: 13:20
 */
?>

<div class="block-header">
    <h2>Gestion des termes</h2>
</div>

<?php
if ($oPage->hasOutputs()) {
    //echo $config->writeOutputs();
}
?>

<div class="card">
    <div class="card-header">
        <h2>Cr&eacute;er<small>Ajout d'un nouveau terme. Assurez-vous que le fichier indiqu&eacute; en tant qu'action existe.</small></h2>
    </div>

    <div class="card-body card-padding">
        <form action="" method="post" class="row" role="form">
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-2">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="zmdi zmdi-link"></i></span>
                        <div class="form-group">
                            <input type="text" name="rule" class="form-control" placeholder="R&egrave;gle regex">
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="zmdi zmdi-globe-alt"></i></span>
                        <div class="form-group">
                            <input type="text" name="location" class="form-control" placeholder="Application">
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="zmdi zmdi-attachment"></i></span>
                        <div class="form-group">
                            <input type="text" name="action" class="form-control" placeholder="Fichier &agrave; inclure">
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="zmdi zmdi-bookmark-outline"></i></span>
                        <div class="form-group">
                            <input type="text" name="lang" class="form-control" placeholder="Nom">
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2">
                    <button type="submit" name="submit" class="btn btn-default">Cr&eacute;er</button>
                </div>
            </div>
            <br />
        </form>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <h2>Liste <small>Liste des URIs existants</small></h2>
    </div>

    <div class="table-responsive">
        <table id="data-table-dictionnary" class="table table-striped">
            <thead>
                <tr>
                    <th data-column-id="<?php echo Term_Manager::$aStructure["iLanguage"]["attname"]; ?>" data-order="desc">Langage</th>
                    <th data-column-id="<?php echo Term_Manager::$aStructure["sAction"]["attname"]; ?>">Action</th>
                    <th data-column-id="<?php echo Term_Manager::$aStructure["sIndex"]["attname"]; ?>">Index</th>
                    <th data-column-id="<?php echo Term_Manager::$aStructure["sValue"]["attname"]; ?>">Valeur</th>
                    <th data-column-id="<?php echo Term_Manager::$aStructure["sApp"]["attname"]; ?>">Application</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script src="<?php echo $oPage->template(); ?>vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<?php echo $oPage->template(); ?>vendors/bower_components/Waves/dist/waves.min.js"></script>
<script src="<?php echo $oPage->template(); ?>vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
<script src="<?php echo $oPage->template(); ?>vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>
<script src="<?php echo $oPage->template(); ?>vendors/bootgrid/jquery.bootgrid.updated.min.js"></script>

<script src="<?php echo $oPage->template(); ?>js/app.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $("#data-table-dictionnary").bootgrid({
            css: {
                icon: 'zmdi icon',
                iconColumns: 'zmdi-view-module',
                iconDown: 'zmdi-sort-amount-desc',
                iconRefresh: 'zmdi-refresh',
                iconUp: 'zmdi-sort-amount-asc'
            },
            ajax: true,
            url: "/apps/admin/script/dictionnary.script.php",
            formatters: {
                "Banned": function(column, row) {
                    return "Oui";
                }
            },
            labels: {
                noResults: "Aucuns résultats",
                loading: "Recherche en cours",
                refresh: "Rafraichir",
                search: "Rechercher",
                infos: "Affichage {{ctx.start}} à {{ctx.end}} sur {{ctx.total}}",
                all: "Tout"
            },
        });
    });
</script>
