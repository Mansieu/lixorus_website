
<?php

function menu ($array)
{
    //echo "<pre>".print_r($array, true)."</pre>\n";
    foreach ($array as $item)
    {
        switch ($item["type"])
        {
            case "menu":
                /*if ($me->getRank()->getAdmin() < $item["cond"]){
                    break;
                }*/

                $toprint  = '<li class="sub-menu">'."\n";
                $toprint .= '<a href="#" data-ma-action="submenu-toggle">';
                $toprint .= (!empty($item["icon"])) ? '<i class="zmdi '.$item["icon"].'"></i>' : "" ;
                $toprint .= $item["lang"].'</a>'."\n".'<ul>';

                echo $toprint;

                menu($item["content"]);

                echo "\n</ul>\n</li>";
                break;
            case "li":
                /*if ($me->getRank()->getAdmin() < $item["cond"]){
                    break;
                }*/

                echo '<li>'."\n\t".'<a href="'.$item["link"].'">'.$item["lang"].'</a>'."\n".'</li>';
                break;
            default:
                break;
        }
    }
}

$aMenu = array(
    array( //home
        "type" => "li",
        "link" => "/admin",
        "lang" => "Home",
        "cond" => 1,
    ),
    array( //application
        "content" => array(
            array(
                "content" => array(
                    array(
                        "type" => "li",
                        "link" => "/admin/accounts",
                        "lang" => "Comptes",
                        "cond" => 1,
                    ),
                    array(
                        "type" => "li",
                        "link" => "/admin/characters",
                        "lang" => "Personnages",
                        "cond" => 1,
                    ),
                ),
                "type" => "menu",
                "lang" => "Joueur",
                "icon" => "zmdi-accounts",
                "cond" => 1,
            ),
            array(
                "content" => array(
                    array(
                        "type" => "li",
                        "link" => "/admin/items",
                        "lang" => "Equipements",
                        "cond" => 2,
                    ),
                ),
                "type" => "menu",
                "lang" => "Contenu",
                "icon" => "zmdi-local-florist",
                "cond" => 2,
            ),
        ),
        "type" => "menu",
        "lang" => "Serveur",
        "icon" => "zmdi-widgets",
        "cond" => 1,
    ),
    array( //website
        "content" => array(
            array(
                "type" => "li",
                "link" => "/admin/news",
                "lang" => "News",
                "cond" => 1,
            ),
            array(
                "type" => "li",
                "link" => "/admin/uris",
                "lang" => "URIs",
                "cond" => 2,
            ),
        ),
        "type" => "menu",
        "lang" => "Website",
        "icon" => "zmdi-globe",
        "cond" => 1,
    ),
    array( //staff
        "content" => array(
            array(
                "content" => array(
                    array(
                        "type" => "li",
                        "link" => "/admin/logs",
                        "lang" => "Application",
                        "cond" => 2,
                    ),
                    array(
                        "type" => "li",
                        "link" => "#",
                        "lang" => "Commandes",
                        "cond" => 2,
                    ),
                ),
                "type" => "menu",
                "lang" => "Logs",
                "icon" => "zmdi-eye",
                "cond" => 2,
            ),
            array(
                "content" => array(
                    array(
                        "type" => "li",
                        "link" => "/admin/ranks",
                        "lang" => "Rangs",
                        "cond" => 2,
                    ),
                ),
                "type" => "menu",
                "lang" => "Gestion",
                "icon" => "zmdi-local-cafe",
                "cond" => 2,
            ),
            array(
                "content" => array(
                    array(
                        "type" => "li",
                        "link" => "/admin",
                        "lang" => "Test",
                        "cond" => 2,
                    ),
                ),
                "type" => "menu",
                "lang" => "Information",
                "icon" => "zmdi-menu",
                "cond" => 2,
            ),
        ),
        "type" => "menu",
        "lang" => "Staff",
        "icon" => "zmdi-menu",
        "cond" => 2,
    ),
);

?>

<aside id="sidebar" class="sidebar c-overflow">
    <ul class="main-menu">
        <li class="mm-profile sub-menu">
            <a href="#" data-ma-action="submenu-toggle" class="media">
                <img class="pull-left" src="<?php echo $oPage->template(); ?>img/profile-pics/1.jpg" alt="">
                <div class="media-body">
                    <?php echo "Marcel"; ?>
                </div>
            </a>

            <ul>
                <li>
                    <a href="#">View Profile</a>
                </li>
                <li>
                    <a href="/logout">Logout</a>
                </li>
            </ul>
        </li>
        <?php menu($aMenu); ?>
    </ul>
</aside>



<aside id="sidebar" class="sidebar c-overflow">
    <ul class="main-menu">
        <li class="mm-profile sub-menu">
            <a href="#" data-ma-action="submenu-toggle" class="media">
                <img class="pull-left" src="http://127.0.0.1/templates/lumos/admin/img/profile-pics/1.jpg" alt="">
                <div class="media-body">Marcel</div>
            </a>

            <ul>
                <li>
                    <a href="#">View Profile</a>
                </li>
                <li>
                    <a href="/logout">Logout</a>
                </li>
            </ul>
        </li>

        <li>
            <a href="/admin">Home</a>
        </li>

        <li class="sub-menu">
            <a href="#" data-ma-action="submenu-toggle">
                <i class="zmdi zmdi-widgets"></i>Serveur
            </a>
            <ul>
                <li class="sub-menu">
                    <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-accounts"></i>Joueur</a>
                    <ul>
                        <li>
                            <a href="/admin/accounts">Comptes</a>
                        </li>
                        <li>
                            <a href="/admin/characters">Personnages</a>
                        </li>
                    </ul>
                </li>
                <li class="sub-menu">
                    <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-local-florist"></i>Contenu</a>
                    <ul>
                        <li>
                            <a href="/admin/items">Equipements</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li><li class="sub-menu">
            <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-globe"></i>Website</a>
            <ul><li>
                    <a href="/admin/news">News</a>
                </li><li>
                    <a href="/admin/uris">URIs</a>
                </li>
            </ul>
        </li><li class="sub-menu">
            <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-menu"></i>Staff</a>
            <ul><li class="sub-menu">
                    <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-eye"></i>Logs</a>
                    <ul><li>
                            <a href="/admin/logs">Application</a>
                        </li><li>
                            <a href="#">Commandes</a>
                        </li>
                    </ul>
                </li><li class="sub-menu">
                    <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-local-cafe"></i>Gestion</a>
                    <ul><li>
                            <a href="/admin/ranks">Rangs</a>
                        </li>
                    </ul>
                </li><li class="sub-menu">
                    <a href="#" data-ma-action="submenu-toggle"><i class="zmdi zmdi-menu"></i>Information</a>
                    <ul><li>
                            <a href="/admin">Test</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
    </ul>
</aside>
