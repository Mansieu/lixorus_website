
<div class="container">
    <section id="404">
        <div class="box">
            <div class="row">
                <div class="col-md-5 col-md-push-7 col-sm-12">
                    <figure class="frame file-not-found">
                        <img src="templates/<?php echo $oPage->getConfig()->getTheme()."/".$oPage->getUrl()->getApp() ?>/images/art/test.png" alt="">
                    </figure>
                </div>
                <div class="col-md-7 col-md-pull-5 col-sm-12">
                    <h2 class="section-title">404 : <?php echo Dict::get("404:title"); ?></h2>
                    <p class="lead"><?php echo Dict::get("404:lead"); ?></p>
                    <p><?php echo Dict::get("404:text"); ?></p>
                    <a href="/" class="btn" style="width: 100%"><?php echo Dict::get("404:link"); ?></a>
                </div>
            </div>

        </div>
    </section>

    <?php include($oPage->get("basic", "footer")); ?>
</div>

<script src="./templates/<?php echo $oPage->getConfig()->getTheme()."/".$oPage->getUrl()->getApp(); ?>/js/bootstrap.min.js"></script>
<script src="./templates/<?php echo $oPage->getConfig()->getTheme()."/".$oPage->getUrl()->getApp(); ?>/js/jquery.themepunch.tools.min.js"></script>
<script src="./templates/<?php echo $oPage->getConfig()->getTheme()."/".$oPage->getUrl()->getApp(); ?>/js/classie.js"></script>
<!-- plugin.js gère le backstrech -->
<script src="./templates/<?php echo $oPage->getConfig()->getTheme()."/".$oPage->getUrl()->getApp(); ?>/js/plugins.js"></script>
<script src="./templates/<?php echo $oPage->getConfig()->getTheme()."/".$oPage->getUrl()->getApp(); ?>/js/scripts.js"></script>

<script>
$.backstretch(["./templates/lumos/website/images/art/b1-13.jpg"]);
</script>
