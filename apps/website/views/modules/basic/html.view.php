<div class="html_section">
    <h2>HTML basics :</h2>
    <p>Le HTML est un langage de balise. Tout ce qui est affiché doit donc forcément être entre deux balises : <b>il ne faut jamais rien avoir en dehors de balises</b>. Certains navigateurs afficheront quand même ce qui était voulu, mais d'autres pourraient ne pas le faire, et ils auraient raison. Le HTML n'est pas un langage qui permettra de changer l'apparence d'une page web, même si la plupart des navigateurs ont des design par défaut qui lui sont attribués. Le HTML permet de structurer la page : avec les balises, on indique où l'on souhaite avoir du texte; avec du css, on le met en forme.</p>
    <p>Les balises suivantes sont les plus utilis&eacute;es :</p>
    <ul>
        <li>
            <h3>Les balises &lt;p&gt;</h3>
            <pre>&lt;p&gt;blablabla&lt;/p&gt;<p>blabla</p></pre>
            <p>Elles permettent d'écrire du texte sur une page. P comme paragraphe.</p>
        </li>
        <li>
            <h3>Les balises &lt;h&gt;</h3>
            <pre>&lt;h1&gt;blablabla&lt;/h1&gt;<h1>blabla</h1></pre>
            <pre>&lt;h2&gt;blablabla&lt;/h2&gt;<h2>blabla</h2></pre>
            <p>Elles sont utilisées pour signaler un titre. On peut aller de h1 à h6. Ces nombres permettent de respecter un ordre d'importance, comme avec des cours : h1 correspond au titre du chapitre, h2 au grand I, h3 au petit 1, etc...</p>
        </li>
        <li>
            <h3>Les balises &lt;b&gt;</h3>
            <pre>&lt;b&gt;blablabla&lt;/b&gt; - <b>blabla</b></pre>
            <pre>&lt;u&gt;blablabla&lt;/u&gt; - <u>blabla</u></pre>
            <pre>&lt;i&gt;blablabla&lt;/i&gt; - <i>blabla</i></pre>
            <pre>&lt;span class="bold"&gt;blablabla&lt;/span&gt; - <span class="bold">blabla</span></pre>
            <p>Les balises b, i et u permettent de mettre des éléments d'un texte en valeur. Originellement, b signifiait gras, u souligné et i italique. Il est aujourd'hui possible de modifier l'apparence de ces balises avec du CSS. Alternativement, on peut utiliser une balise span et lui donner une classe.</p>
        </li>
        <li>
            <h3>Les balises &lt;ul&gt; et &lt;ol&gt;</h3>
            <pre>&lt;ul&gt;blablabla&lt;/ul&gt;<ul><li>blabla</li><li>blabla</li></ul></pre>
            <pre>&lt;ol&gt;blablabla&lt;/ol&gt;<ol><li>blabla</li><li>blabla</li></ol></pre>
            <p></p>
        </li>
    </ul>
</div>