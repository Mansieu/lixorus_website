<?php

include ("../../../config/ajaxenv.inc.php");
include (MODELS_SERVER_MANAGER."map/map.manager.php");

ini_set('memory_limit', '-1');

$i = 0;
$aDir = scandir("../../../data/maps");
$sTableName = "maps";
$sQuery = "SELECT * FROM $sTableName WHERE :file LIKE CONCAT(id,'_',date,'X.swf')";

foreach ($aDir as $sFile){
    $oMap = Map_Manager::query($DATABASE_WEBSITE, $sQuery, array(":file" => $sFile));

    if(!$oMap){
        echo "can't find file : ".$sFile;
    }
}

?>