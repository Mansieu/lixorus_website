
<div class="col-md-6">
    <img src="ressources/home/dofus.jpg" alt="dofus" />
</div>

<p class="lead">It had been in Alex's mind all along—the final minutes on the plane after Damian Cray had died.</p>
<p>Yassen Gregorovich had been there, the Russian assassin who had cast a shadow over so much of Alex's life. Yassen had been dying, a bullet lodged in his chest. But just before the end he'd managed to blurt out a secret that had been buried for fourteen years. Alex's parents had been killed shortly after he was born and he had been brought up by his father's brother, Ian Rider. Earlier this year, Ian Rider had died too, supposedly in a car accident. It had been the shock of Alex's life to discover that his uncle was actually a spy and had been killed on a mission in Cornwall. That was when MI6 had made their appearance. Somehow they had succeeded in sucking Alex into their world, and he had been working for them ever since.</p>
