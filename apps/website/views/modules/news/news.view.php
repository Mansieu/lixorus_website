<?php
require_once("../../../controllers/news.controller.php");
?>

<div class="news">
    <h2 style="width: 100%;"><?php echo Dict::get("news:latestnews:title"); ?></h2>

    <?php
    $i = 1;
    $b = false;
    if(!empty($aNews)){
        foreach ($aNews as $oNews){
            if((($i + 2) % 3) == 0){
                echo '<div class="row-test">';
                $b = true;
            }
            ?>
                <div class="news-box">
                    <div class="img">
                        <img src="ressources/news/1.png" alt="sanctions"/>
                    </div>
                    <div class="head">
                        <a href="#"><h3 class="title"><?php echo $oNews->getTitle(); ?></h3></a>
                        <span class="date"><?php echo $oNews->getDate(); ?></span>
                    </div>
                    <div class="body">
                        <?php echo $oNews->getSummary(); ?>
                    </div>
                </div>
            <?php
            if(($i % 3) == 0){
                echo "</div>";
                $b = false;
            }
            $i++;
        }
        if($b){
            echo "</div>";
        }
    }else{
    ?>
        <p><?php echo Dict::get("news:article:none"); ?></p>
    <?php
    }
    ?>
</div>
