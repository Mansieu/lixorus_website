<?php
require_once("../../../controllers/rules.controller.php");
?>

<div class="container">
    <div id="tabs" class="tabs">
        <nav>
            <ul>
                <li><a href="#section-1" class="icon-shop"><span><?php echo Dict::get("rules:rules:title"); ?></span></a></li>
                <li><a href="#section-2" class="icon-cup"><span><?php echo Dict::get("rules:sanctions:title"); ?></span></a></li>
                <li><a href="#section-3" class="icon-food"><span><?php echo Dict::get("rules:selling:title"); ?></span></a></li>
                <li><a href="#section-4" class="icon-lab"><span><?php echo Dict::get("rules:cheating:title"); ?></span></a></li>
                <li><a href="#section-5" class="icon-truck"><span><?php echo Dict::get("rules:animation:title"); ?></span></a></li>
            </ul>
        </nav>
        <div class="tabs-content">
            <section id="section-1">
                <div class="">
					<?php echo Dict::get("rules:rules:content"); ?>
                </div>
            </section>
            <section id="section-2">
                <div class="">
					<?php echo Dict::get("rules:sanctions:content"); ?>
                </div>
            </section>
            <section id="section-3">
                <div class="">
					<?php echo Dict::get("rules:selling:content"); ?>
                </div>
            </section>
            <section id="section-4">
                <div class="">
					<?php echo Dict::get("rules:cheating:content"); ?>
                </div>
            </section>
            <section id="section-5">
                <div class="">
					<?php echo Dict::get("rules:animation:content"); ?>
                </div>
            </section>
        </div>
    </div>
</div>
