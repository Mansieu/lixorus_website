<?php
require_once("../../../controllers/ladder.controller.php");
?>

<div class="ladder">
    <table style="width: 100%">
        <thead>
            <tr>
                <th class="rank"><?php echo Dict::get("ladder:table:rank"); ?></th>
                <th class="race-img"></th>
                <th class="name"><?php echo Dict::get("ladder:table:name"); ?></th>
                <th class="race"><?php echo Dict::get("ladder:table:race"); ?></th>
                <th class="level"><?php echo Dict::get("ladder:table:level"); ?></th>
                <th class="guild"><?php echo Dict::get("ladder:table:guild"); ?></th>
                <th class="exp"><?php echo Dict::get("ladder:table:experience"); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php for($i = 1; $i < 21; $i++) {?>
                <tr>
                    <td class="rank"><?php echo $i; ?></td>
                    <td class="race-img"><img src="../../../ressources/ladder/race/eni_f.png" alt="race and gender" /></td>
                    <td class="name">Marcel-lebogoce</td>
                    <td class="race">Eniripsa</td>
                    <td class="level">130</td>
                    <td class="guild">Wshlameilleur</td>
                    <td class="exp">250143344334</td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
