<?php
require_once("../../../controllers/ladder.controller.php");
?>

<div class="ladder">
    <table style="width: 100%">
        <thead>
            <tr>
                <th class="rank"><?php echo Dict::get("ladder:table:rank"); ?></th>
                <th class="name"><?php echo Dict::get("ladder:table:name"); ?></th>
                <th class="level"><?php echo Dict::get("ladder:table:level"); ?></th>
                <th class="name"><?php echo Dict::get("ladder:table:head"); ?></th>
                <th class="members"><?php echo Dict::get("ladder:table:members"); ?></th>
                <th class="exp"><?php echo Dict::get("ladder:table:experience"); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php for($i = 1; $i < 21; $i++) {?>
                <tr>
                    <td class="rank"><?php echo $i; ?></td>
                    <td class="name">Wshlameilleur</td>
                    <td class="level">130</td>
                    <td class="name">Marcel-lebogoce</td>
                    <td class="members">12</td>
                    <td class="exp">250143344334</td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
