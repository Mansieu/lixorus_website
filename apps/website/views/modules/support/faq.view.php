
<div class="support-faq">
    <article>
        <h2>Sanction</h2>
        <details>
            <summary>J'ai été témoin d'un infraction</summary>
            <details>
                <summary>Getting Started</summary>
                <p>1. Signup for a free trial</p>
            </details>
            <details>
                <summary>Je souhaite le signaler</summary>
                <p>Vous pouvez signaler une infraction de deux manières :</p>
                <ul>
                    <li>Contacter un modérateur en jeu : si l'infraction est en cours<br />
                        Vous pouvez contacter par message un modérateur pour lui indiquer où est en train de se passer l'infraction pour qu'il puisse s'y rendre. Il est conseillé de n'utiliser cette méthode que si l'infraction est en cours ou très récente. Si les faits sont passés, il vous faudra prendre un screenshot pour montrer au modérateur quelle infraction a été commise.</li>
                    <li>
                        Contacter le support : si l'infraction est passée<br />
                        Vous pouvez envoyer un ticket au support en passant par le site web.
                    </li>
                </ul>
            </details>
        </details>
        <details>
            <summary>J'ai été sanctionné sans aucune raison</summary>
            <p>Si vous pensez avoir été sanctionné à tord, vous pouvez contacter le support en envoyant un ticket. Un membre de l'équipe différent de l'auteur de la sanction s'occupera de votre plainte.</p>
        </details>
    </article>

    <article>
        <h2>SECOND BIG ASS TITLE</h2>
        <details>
            <summary>SERVER Step by Step Guides</summary>
            <details>
                <summary>Getting Started</summary>
                <p>1. Signup for a free trial</p>
            </details>
            <details>
                <summary>Setting up a backup schedule</summary>
                <p>This step assumes you have already signed up and installed the software</p>
            </details>
        </details>
        <details>
            <summary>Installation/Upgrade Issues</summary>
            <p>After setup the program doesn't start</p>
        </details>
    </article>
</div>
