<?php
require_once("../../../controllers/support-new.controller.php");
?>

<div class="support-form">
    <div class="form">
        <form class="cbp-mc-form">
            <label for="type"><?php echo Dict::get("support-new:form:ticket-type:title"); ?></label>
            <select id="type" name="type" data-app="website" data-module="support", data-file="form" onchange='loadSub(this)'>
                <option id="0"><?php echo Dict::get("support-new:form:ticket-type:label"); ?></option>
                <option value="1"><?php echo Dict::get("support-new:form:ticket-type:value:bug"); ?></option>
                <option value="2"><?php echo Dict::get("support-new:form:ticket-type:value:problem-buying"); ?></option>
                <option value="3"><?php echo Dict::get("support-new:form:ticket-type:value:offense"); ?></option>
                <option value="4"><?php echo Dict::get("support-new:form:ticket-type:value:offense-team"); ?></option>
                <option value="5"><?php echo Dict::get("support-new:form:ticket-type:value:contest"); ?></option>
                <option value="6"><?php echo Dict::get("support-new:form:ticket-type:value:ask"); ?></option>
            </select>
            <div class="sub-content"></div>
            <span class="precisions"><p><?php echo Dict::get("support-new:spam-warning"); ?></p></span>
            <div class="cbp-mc-submit-wrap"><input class="cbp-mc-submit" type="submit" value="<?php echo Dict::get("support-new:form:send"); ?>" /></div>
        </form>
    </div>
</div>
