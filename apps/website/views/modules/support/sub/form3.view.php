
<label for="name">Pseudonyme du personnage concerné</label>
<input type="text" id="name" name="name" placeholder="Marcel-lebogoce">
<label for="time">Date et heure (aussi précisément que possible)</label>
<input type="text" id="time" name="time" placeholder="Mardi 12 Juillet 1962 à 21h26 et 32 secondes">
<label for="location">Id de la carte et de la cellule concernée (et/ou géoposition)</label>
<input type="text" id="location" name="location" placeholder="Mapid : 666, Cellid : 42, Pos : 666,666, derrière une des portes">
<label for="screenshot">Screenshot</label>
<input type="text" id="screenshot" name="screenshot" placeholder="http://i50.tinypic.com/w7ezo9.png">
<span class="precisions">
    <p>Une image de l'infraction est obligatoire. Elle doit faire la taille entière de l'écran, ne pas être modifiée (pas d'encadrement de messages dans le chat, pas de censure de pseudonymes, pas de dessins, etc...). Avant de prendre votre screenshot, utilisez les commandes <b>/mapid</b>, <b>/cellid</b> et <b>/time</b> dans le chat.</p>
    <p>Vous pouvez héberger votre image sur un site comme <a href="https://imgbb.com/">celui-ci</a> ou <a href="https://www.noelshack.com/">celui-là</a>.</p>
</span>
<label for="description">Description</label>
<textarea id="description" name="description"></textarea>
