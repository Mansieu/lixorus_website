<?php
require_once("../../../../controllers/support-new.controller.php");
?>

<label for="name"><?php echo Dict::get("support-new:form:nickname:label"); ?></label>
<input type="text" id="name" name="name" placeholder="<?php echo Dict::get("support-new:form:nickname:placeholder"); ?>">
<label for="time"><?php echo Dict::get("support-new:form:time:label"); ?></label>
<input type="text" id="time" name="time" placeholder="<?php echo Dict::get("support-new:form:time:placeholder"); ?>">
<label for="location"><?php echo Dict::get("support-new:form:location:label"); ?></label>
<input type="text" id="location" name="location" placeholder="<?php echo Dict::get("support-new:form:location:placeholder"); ?>">
<label for="screenshot"><?php echo Dict::get("support-new:form:screenshot:label"); ?></label>
<input type="text" id="screenshot" name="screenshot" placeholder="<?php echo Dict::get("support-new:form:screenshot:placeholder"); ?>">
<label for="description"><?php echo Dict::get("support-new:form:description:label"); ?></label>
<textarea id="description" name="description"></textarea>



INSERT INTO `dictionnary`(`language`, `aindex`, `lang`, `action`, `app`)
VALUES
(1, 'support-new:form:nickname:label', 'Pseudonyme du personnage concern&eacute;', 'support-new:', 'website'),
(1, 'support-new:form:nickname:placeholder', 'Marcel-lebogoce', 'support-new:', 'website'),
(1, 'support-new:form:time:label', 'Date et heure (aussi pr&eacute;cis&eacute;ment que possible)', 'support-new:', 'website'),
(1, 'support-new:form:time:placeholder', 'Mardi 12 Juillet 1962 &agrave; 21h26 et 32 secondes', 'support-new:', 'website'),
(1, 'support-new:form:location:label', 'Id de la carte et de la cellule concern&eacute;e (et/ou g&eacute;oposition)', 'support-new:', 'website'),
(1, 'support-new:form:location:placeholder', 'Mapid : 666, Cellid : 42, Pos : 666,666, derri&egrave;re une des portes', 'support-new:', 'website'),
(1, 'support-new:form:screenshot:label', 'Screenshot', 'support-new:', 'website'),
(1, 'support-new:form:screenshot:placeholder', 'http://i50.tinypic.com/w7ezo9.png', 'support-new:', 'website'),
(1, 'support-new:form:description:label', 'Description', 'support-new:', 'website')
;
