<?php
require_once("../../../controllers/login.controller.php");
?>

<div class="login">
    <div class="box">
        <h2 class="section-title"><?php echo Dict::get("login:title"); ?></h2>
        <div class="divide20"></div>
        <?php

        //if ($config->hasOutputs()) {
            //echo $config->writeOutputs();
            echo '<div class="divide20"></div>';
        //}

        ?>
        <form action="" method="post">
            <div class="row">
                <input type="text" name="account"<?php if (isset($_POST['account'])) {
            echo ' value="'.$_POST['account'].'"';
        } ?> class="text-input defaultText required" placeholder="<?php echo Dict::get("login:formular:account"); ?>"/>
                <input type="password" name="pass"<?php if (isset($_POST['pass'])) {
            echo ' value="'.$_POST['pass'].'"';
        } ?> class="text-input defaultText required" placeholder="<?php echo Dict::get("login:formular:password"); ?>"/>
                <input type="submit" value="Valider" name="submit" class="submit" />
            </div>
        </form>
    </div>
    <div class="box">
        <h2 class="section-title"><?php echo Dict::get("signin:title"); ?></h2>
        <div class="divide20"></div>
        <?php

        if ($oPage->hasOutputs()) {
            //echo $oPage->writeOutputs();
            echo '<div class="divide20"></div>';
        }

        ?>
        <form action="" method="post">
			<div class="row">
				<input id="name" type="text" name="name"<?php if (isset($_POST['name'])) {
            echo ' value="'.$_POST['name'].'"';
        } ?> class="text-input defaultText required" placeholder="<?php echo Dict::get("signin:formular:account"); ?>"/>
				<input type="password" name="pass"<?php if (isset($_POST['pass'])) {
            echo ' value="'.$_POST['pass'].'"';
        } ?> class="text-input defaultText required" placeholder="<?php echo Dict::get("signin:formular:password"); ?>"/>
				<input type="password" name="rpass"<?php if (isset($_POST['rpass'])) {
            echo ' value="'.$_POST['rpass'].'"';
        } ?> class="text-input defaultText required" placeholder="<?php echo Dict::get("signin:formular:password2"); ?>"/>
				<input id="pseudo" type="text" name="pseudo"<?php if (isset($_POST['pseudo'])) {
            echo ' value="'.$_POST['pseudo'].'"';
        } ?> class="text-input defaultText required" placeholder="<?php echo Dict::get("signin:formular:nickname"); ?>"/>
				<input type="text" name="email"<?php if (isset($_POST['email'])) {
            echo ' value="'.$_POST['email'].'"';
        } ?> class="text-input defaultText required" placeholder="<?php echo Dict::get("signin:formular:email"); ?>"/>
				<input type="text" name="question"<?php if (isset($_POST['question'])) {
            echo ' value="'.$_POST['question'].'"';
        } ?> class="text-input defaultText required" placeholder="<?php echo Dict::get("signin:formular:secretquestion"); ?>"/>
				<input type="text" name="answer"<?php if (isset($_POST['answer'])) {
            echo ' value="'.$_POST['answer'].'"';
        } ?> class="text-input defaultText required" placeholder="<?php echo Dict::get("signin:formular:secretanswer"); ?>"/>
				<input type="submit" value="<?php echo Dict::get("signin:formular:submit"); ?>" name="submit" class="submit" />
			</div>
        </form>
    </div>
</div>
