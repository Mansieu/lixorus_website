
<?php if (!Login::isConnected()) { ?>
<div class="sidebox box widget">
    <h3 class="widget-title section-title"><?php echo Dict::get("news:login:label"); ?></h3>
    <form action="/login" method="post">
        <fieldset>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-row text-input-row">
                        <input type="text" name="account" placeholder="Account" class="text-input defaultText required"/>
                    </div>
                    <div class="form-row text-input-row">
                        <input type="password" name="pass" placeholder="Password" class="text-input defaultText required"/>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="button-row pull-right">
                        <input type="submit" value="<?php echo Dict::get("news:login:label"); ?>" name="submit" class="btn btn-submit bm0" />
                    </div>
                </div>
            </div>
        </fieldset>
    </form>
</div>
<?php } ?>
