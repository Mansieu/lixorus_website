<?php
require_once(MODELS_WEBSITE_MANAGER."news/news.manager.php");
$aLatestNews = News_Manager::get($oPage->getDb("w"), $oPage->getLanguage());
?>
<div class="sidebox box widget">
    <h3 class="widget-title section-title"><?php echo Dict::get("news:latestnews:title"); ?></h3>
    <ul class="post-list">
        <?php foreach($aLatestNews as $oLatestNews){ ?>
        <li>
            <figure class="frame pull-left">
                <div class="icon-overlay test">
                    <a href="/news/read/<?php echo $oLatestNews->getId(); ?>">
                        <img src="ressources/news/<?php echo $oLatestNews->getImage(); ?>" alt=""/>
                    </a>
                </div>
            </figure>
            <div class="meta">
                <em>
                    <span class="date"><?php echo $oLatestNews->getDate(); ?></span>
                    <span class="category"><?php //echo $oLatestNews->getType()->getLang(); ?>Devblog</span>
                </em>
                <h5>
                    <a href="/news/read/<?php echo $oLatestNews->getId(); ?>"><?php echo $oLatestNews->getTitle(); ?></a>
                </h5>
            </div>
        </li>
        <?php } ?>
    </ul>
</div>
