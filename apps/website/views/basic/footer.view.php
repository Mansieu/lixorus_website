
<footer class="footer box">
    <p class="pull-left">© 2017 <?php echo $oPage->getTitle(); ?>. All rights reserved. Template by <a href="http://elemisfreebies.com/">elemis</a>, content by Mansieu. <a href="/cgu"><?php echo Dict::get("footer:cgu"); ?></a></p>
    <div class="clearfix"></div>
</footer>
