<?php

//Login::setConnected();

$aMenu = array();
$aMenu[1][] = array("link" => "/"             , "icon" => "home-1"    , "content" => Dict::get("menu:home"));
$aMenu[1][] = array("link" => "/news"         , "icon" => "book-1"    , "content" => Dict::get("menu:news"));
//$aMenu[1][] = array("link" => "/ladder"       , "icon" => "award-2"   , "content" => "Classements");
if (!Login::isConnected()) {
    $aMenu[2][] = array("link" => "/login"    , "icon" => "author"    , "content" => Dict::get("menu:login"));
}else{
    $aMenu[3][] = array("link" => "/account"  , "icon" => "profile"   , "content" => "Gestion de compte");
    $aMenu[3][] = array("link" => "http://dl.sidis.online/Launcher/setup.exe", "icon" => "download-1", "content" => "Uplauncher");
    $aMenu[3][] = array("link" => "/logout"   , "icon" => "power"     , "content" => "D&eacute;connexion");
    if(Login::isStaff()){
        $aMenu[3][] = array("link" => "/admin", "icon" => "wrench"    , "content" => "Pannel");
    }
    $aMenu[5][] = array("link" => "/support"  , "icon" => "setting"   , "content" => "Support");
}

foreach($aMenu as $iSubMenu => $aSubMenu){
    foreach($aSubMenu as $iItem => $aItem){
        $aMenu[$iSubMenu][$iItem]  = '<li>';
        $aMenu[$iSubMenu][$iItem] .= '<a href="'.$aItem['link'].'" class="hint--right" data-hint="'.$aItem['content'].'">';
        $aMenu[$iSubMenu][$iItem] .= '<i class="budicon-'.$aItem['icon'].'"></i><span>'.$aItem['content'].'</span>';
        $aMenu[$iSubMenu][$iItem] .= '</a>';
        $aMenu[$iSubMenu][$iItem] .= '</li>';
    }
    $aMenu[$iSubMenu] = implode("\n\t\t\t", $aMenu[$iSubMenu]);
}

$sMenu = "\t\t\t".implode("\n\t\t\t<hr />\n\t\t\t", $aMenu)."\n";

?>

<nav class="navbar navbar-default" role="navigation">
    <div class="navbar-header">
        <a class="btn responsive-menu" data-toggle="collapse" data-target=".navbar-collapse">
            <i></i>
        </a>
        <div class="navbar-brand text-center">
            <a href="/">
                <img src="templates/<?php echo $oPage->getConfig()->getTheme()."/".$oPage->getUrl()->getApp(); ?>/images/egg-2.png" alt="" data-src="templates/<?php echo $oPage->getConfig()->getTheme()."/".$oPage->getUrl()->getApp(); ?>/images/egg-2.png" data-ret="templates/<?php echo $oPage->getConfig()->getTheme()."/".$oPage->getUrl()->getApp(); ?>/images/egg-2.png" class="retina" />
            </a>
        </div>
    </div>

    <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
<?php echo $sMenu; ?>
        </ul>
    </div>
</nav>
