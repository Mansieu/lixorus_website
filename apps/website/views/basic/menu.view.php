<nav id="ml-menu" class="menu">
    <button class="action action--close" aria-label="Close Menu"><span class="icon icon--cross"></span></button>
    <div class="menu__wrap">
        <ul data-menu="main" class="menu__level" tabindex="-1" role="menu" aria-label="TOUT">
            <li class="menu__item" role="menuitem"><a class="menu__link" data-app="website" data-module="news" href="news">ACTUALITE</a></li>
            <li class="menu__item" role="menuitem"><a class="menu__link" data-submenu="submenu-1" aria-owns="submenu-1" href="#">COMMUNAUTE</a></li>
            <li class="menu__item" role="menuitem"><a class="menu__link" data-submenu="submenu-2" aria-owns="submenu-2" href="#">CLASSEMENT</a></li>
            <li class="menu__item" role="menuitem"><a class="menu__link" data-submenu="submenu-3" aria-owns="submenu-3" href="#">ENCYCLOPEDIE</a></li>
            <li class="menu__item" role="menuitem"><a class="menu__link" data-submenu="submenu-4" aria-owns="submenu-4" href="#">SUPPORT</a></li>
            <li class="menu__item" role="menuitem"><a class="menu__link" data-submenu="submenu-5" aria-owns="submenu-5" href="#">COMPTE</a></li>
        </ul>
        <!-- Submenu 1 -->
        <ul data-menu="submenu-1" id="submenu-1" class="menu__level" tabindex="-1" role="menu" aria-label="COMMUNAUTE">
            <li class="menu__item" role="menuitem"><a class="menu__link" data-app="website" data-module="community" href="rules">REGLEMENT</a></li>
            <!--<li class="menu__item" role="menuitem"><a class="menu__link" data-app="website" data-module="basic" href="home">MODERATION</a></li>-->
            <!--<li class="menu__item" role="menuitem"><a class="menu__link" data-app="website" data-module="basic" href="home">TOURNOIS</a></li>-->
            <li class="menu__item" role="menuitem"><a class="menu__link" data-app="link" href="https://discord.gg/ej6c7bh">DISCORD</a></li>
        </ul>
        <!-- Submenu 2 -->
        <ul data-menu="submenu-2" id="submenu-2" class="menu__level" tabindex="-1" role="menu" aria-label="CLASSEMENT">
            <li class="menu__item" role="menuitem"><a class="menu__link" data-app="website" data-module="ladder" href="ladder_pvm">EXPERIENCE</a></li>
            <li class="menu__item" role="menuitem"><a class="menu__link" data-app="website" data-module="ladder" href="ladder_pvp">AGGRESSION</a></li>
            <li class="menu__item" role="menuitem"><a class="menu__link" data-app="website" data-module="ladder" href="ladder_guild">GUILDE</a></li>
        </ul>
        <!-- Submenu 3 -->
        <ul data-menu="submenu-3" id="submenu-3" class="menu__level" tabindex="-1" role="menu" aria-label="BESTIAIRE">
            <li class="menu__item" role="menuitem"><a class="menu__link" data-app="website" data-module="basic" href="home">CLASSES</a></li>
            <li class="menu__item" role="menuitem"><a class="menu__link" data-app="website" data-module="basic" href="home">BESTIAIRE</a></li>
            <li class="menu__item" role="menuitem"><a class="menu__link" data-app="website" data-module="basic" href="home">EQUIPEMENTS</a></li>
            <li class="menu__item" role="menuitem"><a class="menu__link" data-app="website" data-module="basic" href="home">METIERS</a></li>
        </ul>
        <!-- Submenu 4 -->
        <ul data-menu="submenu-4" id="submenu-4" class="menu__level" tabindex="-1" role="menu" aria-label="SUPPORT">
            <li class="menu__item" role="menuitem"><a class="menu__link" data-app="website" data-module="support" href="new">NOUVEAU</a></li>
            <li class="menu__item" role="menuitem"><a class="menu__link" data-app="website" data-module="support" href="list">LISTE</a></li>
            <li class="menu__item" role="menuitem"><a class="menu__link" data-app="website" data-module="support" href="faq">FAQ</a></li>
        </ul>
        <!-- Submenu 5 -->
        <ul data-menu="submenu-5" id="submenu-5" class="menu__level" tabindex="-1" role="menu" aria-label="COMPTE">
            <?php if(!Login::isConnected()){ ?>
                <li class="menu__item" role="menuitem"><a class="menu__link" data-app="website" data-module="account" href="login">CONNECTION</a></li>
                <li class="menu__item" role="menuitem"><a class="menu__link" data-app="website" data-module="basic" href="home">INSCRIPTION</a></li>
            <?php }else{ ?>
                <li class="menu__item" role="menuitem"><a class="menu__link" data-app="website" data-module="basic" href="home">GESTION</a></li>
                <li class="menu__item" role="menuitem"><a class="menu__link" data-app="website" data-module="basic" href="home">ACHAT DE POINTS</a></li>
            <?php } ?>
            <li class="menu__item" role="menuitem"><a class="menu__link" data-app="website" data-module="basic" href="home">VOTE</a></li>
        </ul>
    </div>
</nav>
