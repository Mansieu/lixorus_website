
<?php if(!isset($oPage)) exit; ?>

<!DOCTYPE html>
<html lang="en" class="no-js">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $oPage->getTitle(); ?></title>
    <meta name="description" content="Blueprint: A basic template for a responsive multi-level menu" />
    <meta name="keywords" content="blueprint, template, html, css, menu, responsive, mobile-friendly" />
    <meta name="author" content="Codrops, Mansieu" />
    <link rel="shortcut icon" href="<?php echo $oPage->template(); ?>images/egg-2.ico">
    <link rel="stylesheet" type="text/css" href="<?php echo $oPage->template(); ?>css/organicfoodicons.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $oPage->template(); ?>css/demo.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $oPage->template(); ?>css/component.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $oPage->template(); ?>css/custom.css" />
    <script src="<?php echo $oPage->template(); ?>js/modernizr-custom.js"></script>
    <script src="<?php echo $oPage->template(); ?>js/jquery.min.js"></script>
    <script src="<?php echo $oPage->template(); ?>js/bootstrap.min.js"></script>
    <script src="<?php echo $oPage->template(); ?>js/cbpFWTabs.js"></script>
</head>

<body>
<!-- Main container -->
<div class="main-container">
    <!-- Blueprint header -->
    <header class="bp-header cf">
        <div class="dummy-logo">
            <div class="dummy-icon foodicon foodicon--banana"></div>
            <h2 class="dummy-heading"><?php echo $oPage->getTitle(); ?></h2>
        </div>
    </header>
    <button class="action action--open" aria-label="Open Menu"><span class="icon icon--menu"></span></button>
    <?php
    if ($sFilePath = $oPage->get("basic", "menu")) {
        include($sFilePath);
    }
    ?>
    <div class="content">
        <p class="info">Choisissez un menu</p>
        <!-- Ajax loaded content here -->
    </div>
</div>
<!-- /view -->
<script src="<?php echo $oPage->template(); ?>js/classie.js"></script>
<script src="<?php echo $oPage->template(); ?>js/dummydata.js"></script>
<script src="<?php echo $oPage->template(); ?>js/main.js"></script>
<script>
    (function() {
        $.ajax({url: "apps/website/views/modules/basic/home.view.php", success: function(result){
            gridWrapper.innerHTML = result;
        }});

        var menuEl = document.getElementById('ml-menu'),
            mlmenu = new MLMenu(menuEl, {
                breadcrumbsCtrl : true, // show breadcrumbs
                initialBreadcrumb : 'Lixorus', // initial breadcrumb text
                backCtrl : false, // show back button
                // itemsDelayInterval : 60, // delay between each menu item sliding animation
                onItemClick: loadData // callback: item that doesn´t have a submenu gets clicked - onItemClick([event], [inner HTML of the clicked item])
            });

        // mobile menu toggle
        var openMenuCtrl = document.querySelector('.action--open'),
            closeMenuCtrl = document.querySelector('.action--close');

        openMenuCtrl.addEventListener('click', openMenu);
        closeMenuCtrl.addEventListener('click', closeMenu);

        function openMenu() {
            classie.add(menuEl, 'menu--open');
            closeMenuCtrl.focus();
        }

        function closeMenu() {
            classie.remove(menuEl, 'menu--open');
            openMenuCtrl.focus();
        }

        // simulate grid content loading
        var gridWrapper = document.querySelector('.content');

        function loadData(ev, itemName) {
            ev.preventDefault();

            closeMenu();
            gridWrapper.innerHTML = '';
            classie.add(gridWrapper, 'content--loading');

            var fileName = ev.target.getAttribute('href');
            var appName = ev.target.getAttribute('data-app');
            var moduleName = ev.target.getAttribute('data-module');

            if(appName != "link"){
                setTimeout(function() {
                    classie.remove(gridWrapper, 'content--loading');
                    $.ajax({url: "apps/" + appName + "/views/modules/" + moduleName + "/" + fileName + ".view.php",
                        success: function(result){
                            gridWrapper.innerHTML = result;
                            if(fileName == 'rules'){
                                new CBPFWTabs(document.getElementById('tabs'));
                            }
                        },
                        error: function(result){
                            gridWrapper.innerHTML = "Hep hep hep ! Cette page n'est pas autorisée, on s'balade pas comme on veut ici.";
                        }
                    });
                }, 700);
            }
        }
    })();
</script>
<script>
    function loadSub(ev) {
        var sub = document.querySelector('.sub-content');

        sub.innerHTML = '';

        var fileName = ev.getAttribute('data-file') + ev.value;
        var appName = ev.getAttribute('data-app');
        var moduleName = ev.getAttribute('data-module');

        setTimeout(function() {
            $.ajax({url: "apps/" + appName + "/views/modules/" + moduleName + "/sub/" + fileName + ".view.php",
                success: function(result){
                    sub.innerHTML = result;
                },
                error: function(result){
                    sub.innerHTML = "Erreur de chargement";
                }
            });
        }, 700);
    }
</script>
</body>
</html>
