<!DOCTYPE html>
<html lang="<?php //echo $oPage->getLanguage(); ?>" xml:lang="<?php //echo $oPage->getLanguage(); ?>" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Mansieu">
        <link rel="shortcut icon" href="<?php echo $oPage->template(); ?>images/egg-2.ico">

        <title><?php echo $oPage->getTitle(); ?></title>

        <link rel="stylesheet" type="text/css" href="<?php echo $oPage->template(); ?>css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $oPage->template(); ?>css/plugins.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $oPage->template(); ?>css/prettify.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $oPage->template(); ?>css/custom.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $oPage->template(); ?>css/style.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $oPage->template(); ?>css/color/orange.css">
        <link rel="stylesheet" type="text/css" href='http://fonts.googleapis.com/css?family=Raleway:400,800,700,600,500,300'>
        <link rel="stylesheet" type="text/css" href='http://fonts.googleapis.com/css?family=Libre+Baskerville:400,400italic'>
        <link rel="stylesheet" type="text/css" href="<?php echo $oPage->template(); ?>type/fontello.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $oPage->template(); ?>type/budicons.css">

        <script type="text/javascript" src="<?php echo $oPage->template(); ?>js/html5shiv.js"></script>
        <script type="text/javascript" src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <script type="text/javascript" src="<?php echo $oPage->template(); ?>js/jquery.min.js"></script>
    </head>
    <body class="full-layout">
        <div class="body-wrapper">
            <?php
            if ($sFilePath = $oPage->get("basic", "menu")) {
                include($sFilePath);
            }
            if ($oPage->getUrl()->check("controller")) {
                include($oPage->get("controller"));
            }
            if ($sFilePath = $oPage->get("view")) {
                include($sFilePath);
            }

            //include($oPage->get("js"));
            ?>
        </div>
    </body>
</html>
