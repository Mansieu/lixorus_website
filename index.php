<?php
error_reporting(0);
ini_set('display_errors', 0);

require_once("config/environnement.inc.php");
require_once(MODELS_WEBSITE_CLASS."page/page.inc.php");
require_once(MODELS_WEBSITE_CLASS."config/login.inc.php");
require_once(MODELS_WEBSITE_CLASS."config/utils.inc.php");

Login::setConnected();

if(!isset($_COOKIE['language'])){
    Utils::setCookie(array("name" => 'language', "value" => "2"));
}else{
    Utils::setCookie(array("name" => 'language', "value" => $_COOKIE['language']));
}

$oPage = new Page($DATABASE_WEBSITE);

if(isset($_COOKIE['language'])){
    $oPage->setLanguage($_COOKIE['language']);
}else{
    $oPage->setLanguage("2");
}

//todo
include(APPS."website"."/layout.view.php");

?>
