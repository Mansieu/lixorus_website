-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Ven 19 Janvier 2018 à 13:08
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `website`
--

-- --------------------------------------------------------

--
-- Structure de la table `account`
--

CREATE TABLE `account` (
  `id` int(10) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `code` varchar(15) NOT NULL,
  `rank` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `buy_logs`
--

CREATE TABLE `buy_logs` (
  `id` int(11) NOT NULL,
  `account` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `news` int(11) NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `author` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `config`
--

CREATE TABLE `config` (
  `name` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `theme` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `maintenance` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Contenu de la table `config`
--

INSERT INTO `config` (`name`, `theme`, `maintenance`) VALUES
('NEB', 'lumos', 0);

-- --------------------------------------------------------

--
-- Structure de la table `dictionnary`
--

CREATE TABLE `dictionnary` (
  `language` int(2) NOT NULL,
  `aindex` varchar(255) NOT NULL,
  `lang` text NOT NULL,
  `action` varchar(255) NOT NULL,
  `app` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `dictionnary`
--

INSERT INTO `dictionnary` (`language`, `aindex`, `lang`, `action`, `app`) VALUES
(2, '404:title', 'These are not the droids you are looking for', '404', 'website'),
(2, '404:lead', 'Sometimes even the best people get lost. But don\'t worry, I get it. That map makes no sense to me neither. Maybe if you tilt it to the left... ?', '404', 'website'),
(2, '404:text', 'Obviously, this ain\'t where you wanted to go. The page is no longer available, or is currently either under construction or modification. This means you can\'t access it for the moment. For more information about it, you can contact the owner of the website. And before you ask, no, we do not have pages with cute cat pictures, you\'ve got the wrong website alltogether.', '404', 'website'),
(2, 'base:title', 'File not found', '404', 'website'),
(2, 'footer:cgu', 'Terms of use', 'footer', 'website'),
(2, 'menu:home', 'Home', 'menu', 'website'),
(2, 'menu:news', 'News', 'menu', 'website'),
(2, 'menu:login', 'Log in', 'menu', 'website'),
(2, 'menu:signin', 'Sign in', 'menu', 'website'),
(2, '404:link', 'Go back home', '404', 'website'),
(1, '404:title', 'Ce ne sont pas les droides que vous recherchez', '404', 'website'),
(1, 'base:title', 'Fichier introuvable', '404', 'website'),
(1, 'footer:cgu', 'Condition g&eacute;n&eacute;rale d\'utilisation', 'footer', 'website'),
(1, 'menu:home', 'Accueil', 'menu', 'website'),
(1, 'menu:news', 'Actualit&eacute;s', 'menu', 'website'),
(1, 'menu:login', 'Connexion', 'menu', 'website'),
(1, 'menu:signin', 'Inscription', 'menu', 'website'),
(1, '404:link', 'Retourner &agrave; l\'accueil', '404', 'website'),
(1, '404:lead', 'Parfois, m&ecirc;me les meilleurs d\'entre nous se perdent. Pas d\'inqui&eacute;tude, je te comprends. C\'est pas facile de lire une carte. Peut-&ecirc;tre en la penchant un peu vers la gauche... ?', '404', 'website'),
(1, '404:text', 'Manifestement, ce n\'est pas ce que vous recherchiez. Cette page n\'est plus disponible, ou est en cours de contruction ou modification. Elle n\'est donc pour le moment pas accessible. Pour plus d\'informations, vous pouvez contacter le propri&eacute;taire du site. Et avant que vous le demandiez, non, nous n\'avons pas de page avec des photos de chat mignonnes, vous vous &ecirc;tes tromp&eacute; de site.', '404', 'website'),
(2, 'base:title', 'Home', 'home', 'website'),
(1, 'base:title', 'Accueil', 'home', 'website'),
(1, 'base:title', 'Actualit&eacute;s', 'news', 'website'),
(2, 'base:title', 'News', 'news', 'website'),
(2, 'news:search:input', 'type and enter', 'news', 'website'),
(1, 'news:search:input', 'taper et entrer', 'news', 'website'),
(2, 'news:login:label', 'Log in', 'news', 'website'),
(1, 'news:login:label', 'Connexion', 'news', 'website'),
(1, 'news:article:none', 'Aucun article disponible', 'news', 'website'),
(2, 'news:article:none', 'No article available', 'news', 'website'),
(1, 'news:latestnews:title', 'Derniers articles', 'news', 'website'),
(2, 'news:latestnews:title', 'Lastest news', 'news', 'website'),
(1, 'news:read', 'Lire', 'news', 'website'),
(2, 'news:read', 'Read', 'news', 'website'),
(2, 'base:title', 'Dashboard', 'home', 'admin'),
(1, 'base:title', 'Tableau de bord', 'home', 'admin');

-- --------------------------------------------------------

--
-- Structure de la table `language`
--

CREATE TABLE `language` (
  `id` int(11) NOT NULL,
  `lang` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `language`
--

INSERT INTO `language` (`id`, `lang`) VALUES
(1, 'FR - fr'),
(2, 'EN - en'),
(3, 'GE - ge');

-- --------------------------------------------------------

--
-- Structure de la table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `account` int(11) NOT NULL,
  `pseudo` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `lang` text COLLATE latin1_general_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `language` int(20) NOT NULL DEFAULT '1',
  `author` int(11) NOT NULL,
  `title` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `summary` varchar(600) COLLATE latin1_general_ci NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Contenu de la table `news`
--

INSERT INTO `news` (`id`, `type`, `language`, `author`, `title`, `summary`, `content`, `date`) VALUES
(1, 2, 1, 1, 'Curabitur blandit tempus porttitor nullam id dolor nibh ultricies', 'G&eacute;n&eacute;ralement, on utilise un texte en faux latin (le texte ne veut rien dire, il a &eacute;t&eacute; modifi&eacute;), le Lorem ipsum ou Lipsum, qui permet donc de faire office de texte d\'attente. L\'avantage de le mettre en latin est que l\'op&eacute;rateur sait au premier coup d\'oeil que la page contenant ces lignes n\'est pas...', 'G&eacute;n&eacute;ralement, on utilise un texte en faux latin (le texte ne veut rien dire, il a &eacute;t&eacute; modifi&eacute;), le Lorem ipsum ou Lipsum, qui permet donc de faire office de texte d\'attente. L\'avantage de le mettre en latin est que l\'op&eacute;rateur sait au premier coup d\'oeil que la page contenant ces lignes n\'est pas valide, et surtout l\'attention du client n\'est pas d&eacute;rang&eacute;e par le contenu, il demeure concentr&eacute; seulement sur l\'aspect graphique.\r\n\r\nCe texte a pour autre avantage d\'utiliser des mots de longueur variable, essayant de simuler une occupation normale. La m&eacute;thode simpliste consistant &agrave; copier-coller un court texte plusieurs fois (« ceci est un faux-texte ceci est un faux-texte ceci est un faux-texte ceci est un faux-texte ceci est un faux-texte ») a l\'inconv&eacute;nient de ne pas permettre une juste appr&eacute;ciation typographique du r&eacute;sultat final.\r\n\r\nIl circule des centaines de versions diff&eacute;rentes du Lorem ipsum, mais ce texte aurait originellement &eacute;t&eacute; tir&eacute; de l\'ouvrage de Cic&eacute;ron, De Finibus Bonorum et Malorum (Liber Primus, 32), texte populaire &agrave; cette &eacute;poque, dont l\'une des premi&egrave;res phrases est : Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit... (Il n\'existe personne qui aime la souffrance pour elle-m&ecirc;me, ni qui la recherche ni qui la veuille pour ce qu\'elle est...).', '2017-04-28 07:21:59'),
(2, 2, 1, 1, 'Ridiculus ultricies pellentesque francese', 'G&eacute;n&eacute;ralement, on utilise un texte en faux latin (le texte ne veut rien dire, il a &eacute;t&eacute; modifi&eacute;), le Lorem ipsum ou Lipsum, qui permet donc de faire office de texte d\'attente. L\'avantage de le mettre en latin est que l\'op&eacute;rateur sait au premier coup d\'oeil que la page contenant ces lignes n\'est pas...', 'G&eacute;n&eacute;ralement, on utilise un texte en faux latin (le texte ne veut rien dire, il a &eacute;t&eacute; modifi&eacute;), le Lorem ipsum ou Lipsum, qui permet donc de faire office de texte d\'attente. L\'avantage de le mettre en latin est que l\'op&eacute;rateur sait au premier coup d\'oeil que la page contenant ces lignes n\'est pas valide, et surtout l\'attention du client n\'est pas d&eacute;rang&eacute;e par le contenu, il demeure concentr&eacute; seulement sur l\'aspect graphique.\r\n\r\nCe texte a pour autre avantage d\'utiliser des mots de longueur variable, essayant de simuler une occupation normale. La m&eacute;thode simpliste consistant &agrave; copier-coller un court texte plusieurs fois (« ceci est un faux-texte ceci est un faux-texte ceci est un faux-texte ceci est un faux-texte ceci est un faux-texte ») a l\'inconv&eacute;nient de ne pas permettre une juste appr&eacute;ciation typographique du r&eacute;sultat final.\r\n\r\nIl circule des centaines de versions diff&eacute;rentes du Lorem ipsum, mais ce texte aurait originellement &eacute;t&eacute; tir&eacute; de l\'ouvrage de Cic&eacute;ron, De Finibus Bonorum et Malorum (Liber Primus, 32), texte populaire &agrave; cette &eacute;poque, dont l\'une des premi&egrave;res phrases est : Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit... (Il n\'existe personne qui aime la souffrance pour elle-m&ecirc;me, ni qui la recherche ni qui la veuille pour ce qu\'elle est...).', '2017-04-28 07:21:59'),
(3, 2, 1, 1, 'Ceci est un titre1', 'G&eacute;n&eacute;ralement, on utilise un texte en faux latin (le texte ne veut rien dire, il a &eacute;t&eacute; modifi&eacute;), le Lorem ipsum ou Lipsum, qui permet donc de faire office de texte d\'attente. L\'avantage de le mettre en latin est que l\'op&eacute;rateur sait au premier coup d\'oeil que la page contenant ces lignes n\'est pas...', 'G&eacute;n&eacute;ralement, on utilise un texte en faux latin (le texte ne veut rien dire, il a &eacute;t&eacute; modifi&eacute;), le Lorem ipsum ou Lipsum, qui permet donc de faire office de texte d\'attente. L\'avantage de le mettre en latin est que l\'op&eacute;rateur sait au premier coup d\'oeil que la page contenant ces lignes n\'est pas valide, et surtout l\'attention du client n\'est pas d&eacute;rang&eacute;e par le contenu, il demeure concentr&eacute; seulement sur l\'aspect graphique.\r\n\r\nCe texte a pour autre avantage d\'utiliser des mots de longueur variable, essayant de simuler une occupation normale. La m&eacute;thode simpliste consistant &agrave; copier-coller un court texte plusieurs fois (« ceci est un faux-texte ceci est un faux-texte ceci est un faux-texte ceci est un faux-texte ceci est un faux-texte ») a l\'inconv&eacute;nient de ne pas permettre une juste appr&eacute;ciation typographique du r&eacute;sultat final.\r\n\r\nIl circule des centaines de versions diff&eacute;rentes du Lorem ipsum, mais ce texte aurait originellement &eacute;t&eacute; tir&eacute; de l\'ouvrage de Cic&eacute;ron, De Finibus Bonorum et Malorum (Liber Primus, 32), texte populaire &agrave; cette &eacute;poque, dont l\'une des premi&egrave;res phrases est : Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit... (Il n\'existe personne qui aime la souffrance pour elle-m&ecirc;me, ni qui la recherche ni qui la veuille pour ce qu\'elle est...).', '2017-05-03 13:00:54'),
(4, 2, 1, 1, 'Ceci est un titre2', 'G&eacute;n&eacute;ralement, on utilise un texte en faux latin (le texte ne veut rien dire, il a &eacute;t&eacute; modifi&eacute;), le Lorem ipsum ou Lipsum, qui permet donc de faire office de texte d\'attente. L\'avantage de le mettre en latin est que l\'op&eacute;rateur sait au premier coup d\'oeil que la page contenant ces lignes n\'est pas...', 'G&eacute;n&eacute;ralement, on utilise un texte en faux latin (le texte ne veut rien dire, il a &eacute;t&eacute; modifi&eacute;), le Lorem ipsum ou Lipsum, qui permet donc de faire office de texte d\'attente. L\'avantage de le mettre en latin est que l\'op&eacute;rateur sait au premier coup d\'oeil que la page contenant ces lignes n\'est pas valide, et surtout l\'attention du client n\'est pas d&eacute;rang&eacute;e par le contenu, il demeure concentr&eacute; seulement sur l\'aspect graphique.\r\n\r\nCe texte a pour autre avantage d\'utiliser des mots de longueur variable, essayant de simuler une occupation normale. La m&eacute;thode simpliste consistant &agrave; copier-coller un court texte plusieurs fois (« ceci est un faux-texte ceci est un faux-texte ceci est un faux-texte ceci est un faux-texte ceci est un faux-texte ») a l\'inconv&eacute;nient de ne pas permettre une juste appr&eacute;ciation typographique du r&eacute;sultat final.\r\n\r\nIl circule des centaines de versions diff&eacute;rentes du Lorem ipsum, mais ce texte aurait originellement &eacute;t&eacute; tir&eacute; de l\'ouvrage de Cic&eacute;ron, De Finibus Bonorum et Malorum (Liber Primus, 32), texte populaire &agrave; cette &eacute;poque, dont l\'une des premi&egrave;res phrases est : Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit... (Il n\'existe personne qui aime la souffrance pour elle-m&ecirc;me, ni qui la recherche ni qui la veuille pour ce qu\'elle est...).', '2017-05-03 13:02:29'),
(5, 2, 1, 1, 'Ceci est un titre3', 'G&eacute;n&eacute;ralement, on utilise un texte en faux latin (le texte ne veut rien dire, il a &eacute;t&eacute; modifi&eacute;), le Lorem ipsum ou Lipsum, qui permet donc de faire office de texte d\'attente. L\'avantage de le mettre en latin est que l\'op&eacute;rateur sait au premier coup d\'oeil que la page contenant ces lignes n\'est pas...', 'G&eacute;n&eacute;ralement, on utilise un texte en faux latin (le texte ne veut rien dire, il a &eacute;t&eacute; modifi&eacute;), le Lorem ipsum ou Lipsum, qui permet donc de faire office de texte d\'attente. L\'avantage de le mettre en latin est que l\'op&eacute;rateur sait au premier coup d\'oeil que la page contenant ces lignes n\'est pas valide, et surtout l\'attention du client n\'est pas d&eacute;rang&eacute;e par le contenu, il demeure concentr&eacute; seulement sur l\'aspect graphique.\r\n\r\nCe texte a pour autre avantage d\'utiliser des mots de longueur variable, essayant de simuler une occupation normale. La m&eacute;thode simpliste consistant &agrave; copier-coller un court texte plusieurs fois (« ceci est un faux-texte ceci est un faux-texte ceci est un faux-texte ceci est un faux-texte ceci est un faux-texte ») a l\'inconv&eacute;nient de ne pas permettre une juste appr&eacute;ciation typographique du r&eacute;sultat final.\r\n\r\nIl circule des centaines de versions diff&eacute;rentes du Lorem ipsum, mais ce texte aurait originellement &eacute;t&eacute; tir&eacute; de l\'ouvrage de Cic&eacute;ron, De Finibus Bonorum et Malorum (Liber Primus, 32), texte populaire &agrave; cette &eacute;poque, dont l\'une des premi&egrave;res phrases est : Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit... (Il n\'existe personne qui aime la souffrance pour elle-m&ecirc;me, ni qui la recherche ni qui la veuille pour ce qu\'elle est...).', '2017-05-03 13:03:56'),
(1, 2, 2, 1, 'Is this the real life, is this just fantasy ?', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempus accumsan rutrum. Nunc fermentum ac dolor in elementum. Cras in consequat dolor, in tincidunt tortor. Phasellus eu arcu dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed consectetur scelerisque odio id imperdiet metus.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec molestie hendrerit congue. Duis vulputate sagittis ullamcorper. Nulla facilisi. Nam viverra quam a felis tempor, sit amet luctus risus luctus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce elementum eros sem, vitae finibus velit scelerisque et. Pellentesque congue eu erat et consectetur. Etiam dignissim viverra nisl mattis ultrices. Suspendisse mattis elit at purus iaculis, a tempor dolor aliquet. Vestibulum nec pretium eros. Donec tristique ac augue sit amet tempor. Maecenas dictum hendrerit quam, sed lacinia enim lobortis at. Pellentesque placerat fermentum turpis at auctor. Ut condimentum luctus tellus et semper.', '2017-04-28 07:21:59'),
(2, 2, 2, 1, 'Ridiculus ultricies pellentesque inglese', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempus accumsan rutrum. Nunc fermentum ac dolor in elementum. Cras in consequat dolor, in tincidunt tortor. Phasellus eu arcu dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed consectetur scelerisque odio id imperdiet metus.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed porta odio eget neque consectetur, eleifend sagittis ex vehicula. Mauris lacinia ornare nisi, sit amet molestie neque consectetur quis. Praesent ac risus non metus bibendum condimentum in sed est. Duis eu velit ante. Suspendisse placerat est at dui finibus eleifend. Mauris nibh dolor, tempus sed libero nec, accumsan egestas ante. Fusce sagittis, elit at facilisis molestie, purus ex congue nisi, sed commodo libero tellus ut urna. Cras molestie aliquet justo nec varius. Aliquam molestie tortor urna, sit amet convallis enim malesuada bibendum. Donec congue turpis lorem, eget pulvinar augue maximus vitae. Nunc pellentesque, dolor a tempus facilisis, eros ante suscipit sem, ac iaculis nisl quam in enim. Suspendisse vulputate metus lacus, quis convallis ligula hendrerit id.', '2017-04-28 07:21:59'),
(3, 2, 2, 1, 'This is a title1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempus accumsan rutrum. Nunc fermentum ac dolor in elementum. Cras in consequat dolor, in tincidunt tortor. Phasellus eu arcu dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed consectetur scelerisque odio id imperdiet metus.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.   \r\n\r\nDuis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet,', '2017-05-03 13:00:54'),
(4, 2, 2, 1, 'This is a title2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempus accumsan rutrum. Nunc fermentum ac dolor in elementum. Cras in consequat dolor, in tincidunt tortor. Phasellus eu arcu dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed consectetur scelerisque odio id imperdiet metus.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.   \r\n\r\nDuis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet,', '2017-05-03 13:02:29'),
(5, 2, 2, 1, 'This is a title3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempus accumsan rutrum. Nunc fermentum ac dolor in elementum. Cras in consequat dolor, in tincidunt tortor. Phasellus eu arcu dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed consectetur scelerisque odio id imperdiet metus.', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.   \r\n\r\nDuis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet,', '2017-05-03 13:03:56');

-- --------------------------------------------------------

--
-- Structure de la table `news_type`
--

CREATE TABLE `news_type` (
  `id` int(11) NOT NULL,
  `language` int(20) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Contenu de la table `news_type`
--

INSERT INTO `news_type` (`id`, `language`, `name`) VALUES
(1, 1, 'Devblog'),
(2, 1, 'Actualit&eacute;s'),
(3, 1, 'Recrutement'),
(1, 2, 'Devblog'),
(2, 2, 'News'),
(3, 2, 'Recruting');

-- --------------------------------------------------------

--
-- Structure de la table `ranks`
--

CREATE TABLE `ranks` (
  `id` int(11) NOT NULL,
  `lang` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `admin` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Contenu de la table `ranks`
--

INSERT INTO `ranks` (`id`, `lang`, `admin`) VALUES
(1, 'Membre', 0),
(2, 'R&eacute;dacteur', 0),
(3, 'Mod&eacute;rateur', 1),
(4, 'Administrateur', 1);

-- --------------------------------------------------------

--
-- Structure de la table `rights`
--

CREATE TABLE `rights` (
  `rank` int(11) NOT NULL,
  `right` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Contenu de la table `rights`
--

INSERT INTO `rights` (`rank`, `right`) VALUES
(4, 1);

-- --------------------------------------------------------

--
-- Structure de la table `rights_lang`
--

CREATE TABLE `rights_lang` (
  `id` int(11) NOT NULL,
  `lang` varchar(255) COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Contenu de la table `rights_lang`
--

INSERT INTO `rights_lang` (`id`, `lang`) VALUES
(1, 'uris');

-- --------------------------------------------------------

--
-- Structure de la table `shop_logs`
--

CREATE TABLE `shop_logs` (
  `id` int(11) NOT NULL,
  `account` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `themes`
--

CREATE TABLE `themes` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `lang` varchar(255) COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Contenu de la table `themes`
--

INSERT INTO `themes` (`id`, `name`, `lang`) VALUES
(1, 'default', 'Default'),
(2, 'lumos', 'Lumos');

-- --------------------------------------------------------

--
-- Structure de la table `uri`
--

CREATE TABLE `uri` (
  `rule` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `location` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `module` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `action` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `lang` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `rank` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Contenu de la table `uri`
--

INSERT INTO `uri` (`rule`, `location`, `module`, `action`, `lang`, `rank`) VALUES
('^/$', 'website', '', 'home', 'home', 1),
('^/admin/?$', 'admin', '', 'home', 'dashboard', 2),
('^/news/?$', 'website', '', 'news', 'news', 1),
('^/register/?$', 'website', '', 'register', 'register', 1),
('^/login/?$', 'website', '', 'login', 'login', 1),
('^/icons/?$', 'website', '', 'icons', 'icons', 1),
('^/logout/?$', 'website', '', 'logout', 'logout', 1),
('^/admin/uris/?$', 'admin', '', 'uris', 'uris', 1),
('^/admin/news/?$', 'admin', '', 'new', 'news', 1),
('^/admin/logs/?$', 'admin', '', 'logs', 'logs app', 1),
('^/news/read/([0-9]+)$', 'website', '', 'new', 'affichage article', 1),
('^/news/([0-9]+)$', 'website', '', 'home', 'news pages', 1),
('^/account/?$', 'website', '', 'account', 'gestion de compte', 1);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `buy_logs`
--
ALTER TABLE `buy_logs`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`name`);

--
-- Index pour la table `dictionnary`
--
ALTER TABLE `dictionnary`
  ADD PRIMARY KEY (`language`,`aindex`,`action`,`app`);

--
-- Index pour la table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`,`language`);

--
-- Index pour la table `news_type`
--
ALTER TABLE `news_type`
  ADD PRIMARY KEY (`id`,`language`);

--
-- Index pour la table `ranks`
--
ALTER TABLE `ranks`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rights`
--
ALTER TABLE `rights`
  ADD PRIMARY KEY (`rank`,`right`);

--
-- Index pour la table `rights_lang`
--
ALTER TABLE `rights_lang`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `shop_logs`
--
ALTER TABLE `shop_logs`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `themes`
--
ALTER TABLE `themes`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `uri`
--
ALTER TABLE `uri`
  ADD PRIMARY KEY (`rule`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `account`
--
ALTER TABLE `account`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `buy_logs`
--
ALTER TABLE `buy_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `ranks`
--
ALTER TABLE `ranks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `rights_lang`
--
ALTER TABLE `rights_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `shop_logs`
--
ALTER TABLE `shop_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `themes`
--
ALTER TABLE `themes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
