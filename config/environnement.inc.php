<?php

    # file path
define("APPROOT", $_SERVER['DOCUMENT_ROOT']);
define("URL", "http://".$_SERVER['HTTP_HOST']);
define("TEMPLATES", "templates/");
define("APPS", "apps/");
define("MODELS", "models/");
define("CONFIG", "config/");
define("RESSOURCES", "ressources/");

    # models
define("MODELS_WEBSITE", MODELS."website/");
define("MODELS_SERVER", MODELS."server/");
define("MODELS_WEBSITE_CLASS", MODELS_WEBSITE."class/");
define("MODELS_WEBSITE_MANAGER", MODELS_WEBSITE."manager/");
define("MODELS_SERVER_CLASS", MODELS_SERVER."class/");
define("MODELS_SERVER_MANAGER", MODELS_SERVER."manager/");

    # apps
define("CONTROLLERS", "controllers/");
define("VIEWS", "views/");
define("DICTS", "dicts/");
define("SCRIPTS", "scripts/");

    # file extension
define("PHP", ".php");

    # theme
define("THEME_DEFAULT", "default");

    # lang
define("LANG_DEFAULT", "2");

    # databases

require_once(MODELS_WEBSITE_CLASS."database/database.inc.php");

//$DATABASE_WEBSITE = new Database("db734838239.db.1and1.com", "db734838239", "dbo734838239", '7cgF%$h-djQQ6Tye');
$DATABASE_WEBSITE = new Database("localhost","website","root","0123456789");
//$DATABASE_GAME = new Database("localhost","game","root","");
